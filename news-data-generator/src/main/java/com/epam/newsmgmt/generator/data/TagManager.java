package com.epam.newsmgmt.generator.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmgmt.dao.TagDao;
import com.epam.newsmgmt.domain.Tag;

@Component
public class TagManager {
	@Autowired
	private TagDao tagDao;

	public List<Tag> fetchAll() {
		return tagDao.fetchAll();
	}

	public void addTags() {
		String[] tags = { "IT", "art", "sport", "science", "politics", "economics", "people", "health", "travelling",
				"world", "culture", "history", "programming" };
		for (String t : tags) {
			tagDao.insert(new Tag(t));
		}
	}
}
