
package com.epam.newsmgmt.generator.storage;

import java.util.Random;

public class CharacterStorage {

	private static final char[] CONSONANTS = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r',
			's', 't', 'v', 'x', 'z', 'w' };
	private static final char[] VOWELS = { 'e', 'y', 'u', 'i', 'o', 'a' };
	private static final char[] SENTENCE_END_PUNCTUATION = { '.', '!' };

	private static final char[] SENTENCE_MIDDLE_PUNCTUATION = { ',', ':', ';' };

	private static final Random RANDOM = new Random();

	public static char randomVowel() {
		return VOWELS[RANDOM.nextInt(VOWELS.length)];
	}

	public static char randomConsonant() {
		return CONSONANTS[RANDOM.nextInt(CONSONANTS.length)];
	}

	public static char randomSentenceEndPunctuation() {
		double randomValue = Math.random();
		if (randomValue < PunctuationAppearingChance.POINT.getChance()) {
			return SENTENCE_END_PUNCTUATION[0];
		} else {
			return SENTENCE_END_PUNCTUATION[1];
		}
	}

	public static char randomSentenceMiddlePunctuation() {
		double randomValue = Math.random();
		if (randomValue < PunctuationAppearingChance.COMMA.getChance()) {
			return SENTENCE_MIDDLE_PUNCTUATION[0];
		} else if (randomValue < PunctuationAppearingChance.COMMA.getChance()
				+ PunctuationAppearingChance.COLON.getChance()) {
			return SENTENCE_MIDDLE_PUNCTUATION[1];
		} else {
			return SENTENCE_MIDDLE_PUNCTUATION[2];
		}
	}
}