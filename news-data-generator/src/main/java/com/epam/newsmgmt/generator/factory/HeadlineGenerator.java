package com.epam.newsmgmt.generator.factory;

import static com.epam.newsmgmt.generator.storage.WordsStorage.ADJECTIVES;
import static com.epam.newsmgmt.generator.storage.WordsStorage.CONTINUOUS_TIME;
import static com.epam.newsmgmt.generator.storage.WordsStorage.CONTINUOUS_TITLE_CHANCE;
import static com.epam.newsmgmt.generator.storage.WordsStorage.CONTINUOUS_VERBS;
import static com.epam.newsmgmt.generator.storage.WordsStorage.OBJECT;
import static com.epam.newsmgmt.generator.storage.WordsStorage.PLACE;
import static com.epam.newsmgmt.generator.storage.WordsStorage.SIMPLE_TIME;
import static com.epam.newsmgmt.generator.storage.WordsStorage.SIMPLE_VERBS;
import static com.epam.newsmgmt.generator.storage.WordsStorage.SUBJECT;

import java.util.Random;

import com.epam.newsmgmt.generator.storage.CharacterStorage;

public class HeadlineGenerator {

	public static String buildNewsHeadline() {
		if (Math.random() < CONTINUOUS_TITLE_CHANCE) {
			return buildContinuousTitle();
		}
		return buildSimpleTitle();
	}

	private static String buildContinuousTitle() {
		Random random = new Random();
		StringBuilder builder = new StringBuilder();
		int adjectiveIndex = random.nextInt(ADJECTIVES.length);
		int subjectIndex = random.nextInt(SUBJECT.length);
		int verbIndex = random.nextInt(CONTINUOUS_VERBS.length);
		int objectIndex = random.nextInt(OBJECT.length);
		int timeIndex = random.nextInt(CONTINUOUS_TIME.length);
		int placeIndex = random.nextInt(PLACE.length);
		builder.append(ADJECTIVES[adjectiveIndex]);
		builder.append(" ");
		builder.append(SUBJECT[subjectIndex]);
		builder.append(" ");
		builder.append(CONTINUOUS_VERBS[verbIndex]);
		builder.append(" ");
		builder.append(OBJECT[objectIndex]);
		builder.append(" ");
		builder.append(CONTINUOUS_TIME[timeIndex]);
		builder.append(" ");
		builder.append(PLACE[placeIndex]);
		builder.append(CharacterStorage.randomSentenceEndPunctuation());
		String uppercaseFirstLetter = builder.subSequence(0, 1).toString().toUpperCase();
		builder.replace(0, 1, uppercaseFirstLetter);
		return builder.toString();
	}

	private static String buildSimpleTitle() {
		Random random = new Random();
		StringBuilder builder = new StringBuilder();
		int adjectiveIndex = random.nextInt(ADJECTIVES.length);
		int subjectIndex = random.nextInt(SUBJECT.length);
		int verbIndex = random.nextInt(SIMPLE_VERBS.length);
		int objectIndex = random.nextInt(OBJECT.length);
		int timeIndex = random.nextInt(SIMPLE_TIME.length);
		int placeIndex = random.nextInt(PLACE.length);
		builder.append(ADJECTIVES[adjectiveIndex]);
		builder.append(" ");
		builder.append(SUBJECT[subjectIndex]);
		builder.append(" ");
		builder.append(SIMPLE_VERBS[verbIndex]);
		builder.append(" ");
		builder.append(OBJECT[objectIndex]);
		builder.append(" ");
		builder.append(SIMPLE_TIME[timeIndex]);
		builder.append(" ");
		builder.append(PLACE[placeIndex]);
		builder.append(CharacterStorage.randomSentenceEndPunctuation());
		String uppercaseFirstLetter = builder.subSequence(0, 1).toString().toUpperCase();
		builder.replace(0, 1, uppercaseFirstLetter);
		return builder.toString();
	}
}
