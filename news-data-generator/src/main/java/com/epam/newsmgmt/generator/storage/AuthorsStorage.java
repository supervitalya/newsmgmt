package com.epam.newsmgmt.generator.storage;

import java.util.Random;

import com.epam.newsmgmt.domain.Author;

public class AuthorsStorage {
	private static final String[] FIRST_NAMES = { "John", "Jim", "George", "Peter", "Samuel", "Bob", "Daniel", "Leo",
			"Harry", "Jack", "James", "Ron", "Natalie", "Kate", "Jennifer", "Benjamin", "Brandon", "Bradley", "Bruce",
			"Geralt", "Alan", "Adam", "Andrew", "Charles", "Chester", "Cris", "Dan", "Dennis", "Donald", "Edward",
			"Ellit", "Erwin", "Ferdinand", "Floyd", "George", "Gregory" };

	private static final String[] LAST_NAMES = { "Smith", "Jones", "McCartney", "Lennon", "Cooper", "Martin", "Lee",
			"May", "Johnson", "Ali", "Jackson", "Williams", "Abbey", "Abraham", "Adams", "Atkins", "Banes", "Burns",
			"Bentley", "Black", "White", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Thomas", "Harris",
			"Thomson", "Lopez", "Garcia" };

	private static final int AUTHORS_NUMBER = 100;

	private static final Author[] AUTHORS = new Author[AUTHORS_NUMBER];
	private static final Random RANDOM = new Random();

	static {
		for (int i = 0; i < AUTHORS_NUMBER; i++) {
			AUTHORS[i] = new Author(null, FIRST_NAMES[RANDOM.nextInt(FIRST_NAMES.length)],
					LAST_NAMES[RANDOM.nextInt(LAST_NAMES.length)]);
		}
	}

	public static Author[] getAuthors() {
		return AUTHORS;
	}

	public static Author getRandomAuthor() {
		return AUTHORS[RANDOM.nextInt(AUTHORS_NUMBER)];
	}
}
