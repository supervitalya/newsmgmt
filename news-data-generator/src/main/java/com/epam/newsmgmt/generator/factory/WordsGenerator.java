package com.epam.newsmgmt.generator.factory;

import java.util.Random;

import com.epam.newsmgmt.generator.storage.CharacterStorage;

public class WordsGenerator {
	private static final int MIN_LENGTH = 3;
	private static final int MAX_LENGTH = 10;

	public static String generateWord() {
		int length = defineWordLength();
		char[] chars = new char[length - 1];
		for (int i = 0; i < length - 1; i++) {
			if (Math.random() < 0.5) {
				chars[i] = CharacterStorage.randomConsonant();
			} else {
				chars[i] = CharacterStorage.randomVowel();
			}
		}
		return new String(chars);
	}

	private static int defineWordLength() {
		return MIN_LENGTH + new Random().nextInt(MAX_LENGTH - MIN_LENGTH);
	}
}
