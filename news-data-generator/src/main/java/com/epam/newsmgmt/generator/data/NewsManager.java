package com.epam.newsmgmt.generator.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.dao.NewsDao;
import com.epam.newsmgmt.dao.TagDao;
import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.domain.Tag;

@Repository
public class NewsManager {
	@Autowired
	private NewsDao newsDao;
	@Autowired
	private AuthorDao authorDao;
	@Autowired
	private TagDao tagDao;

	@Autowired
	private SessionFactory factory;
	
	private static final int BATCH_SIZE = 50;
	private static final int NEWS_NUMBER = 11000;
	private static final int AUTHORS_MAX_NUMBER = 4;
	private static final int TAGS_MAX_NUMBER = 3;
	
	@Transactional
	public void saveNews(List<News> news) {
		Session session = factory.getCurrentSession();
		for (int i = 0; i < news.size(); i++) {
			session.persist(news.get(i));
			if (i % 50 == 0) {
				session.flush();
				session.clear();
			}
		}
	}

	public List<News> fetchAll() {
		return newsDao.fetchAll(0, Integer.MAX_VALUE);
	}

	@Transactional
	public void addTagsAndAuthorsToNews() {
		List<Author> authors = authorDao.fetchAll();
		List<Tag> tags = tagDao.fetchAll();
		System.out.println(authors.size());
		System.out.println(tags.size());
		for (int i = 0; i < NEWS_NUMBER; i += BATCH_SIZE) {
			System.out.println("batch: " + i);
			List<News> batchNewsList = newsDao.fetchAll(i, BATCH_SIZE);
			fillWithTagsAndAuthors(batchNewsList, authors, tags);
		}
	}

	private void fillWithTagsAndAuthors(List<News> batchNewsList, List<Author> authors, List<Tag> tags) {
		Session session = factory.getCurrentSession();
		for (News n : batchNewsList) {
			News newNews = session.get(News.class, n.getId());
			newNews.setAuthors(createAuthorList(authors));
			newNews.setTags(createTagSet(tags));
			session.update(newNews);
		}
		session.flush();
		session.clear();
	}

	private List<Tag> createTagSet(List<Tag> allTags) {
		Random random = new Random();
		List<Tag> tags = new ArrayList<>();
		for (int i = 0; i < random.nextInt(TAGS_MAX_NUMBER); i++) {
			tags.add(allTags.get(random.nextInt(allTags.size())));
		}
		return tags;
	}

	private List<Author> createAuthorList(List<Author> allAuthors) {
		Random random = new Random();
		List<Author> authors = new ArrayList<>();
		for (int i = 0; i < random.nextInt(AUTHORS_MAX_NUMBER); i++) {
			authors.add(allAuthors.get(random.nextInt(allAuthors.size())));
		}
		return authors;
	}
}
