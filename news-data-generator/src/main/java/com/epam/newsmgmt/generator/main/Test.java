package com.epam.newsmgmt.generator.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmgmt.generator.data.AuthorManager;
import com.epam.newsmgmt.generator.data.CommentManager;
import com.epam.newsmgmt.generator.data.NewsManager;
import com.epam.newsmgmt.generator.data.TagManager;

public class Test {
	private static AuthorManager authorManager;
	private static TagManager tagManager;
	private static NewsManager newsManager;
	private static CommentManager commentManager;
	static {
		ApplicationContext context = new ClassPathXmlApplicationContext("generator-context.xml");
		authorManager = context.getBean(AuthorManager.class);
		tagManager = context.getBean(TagManager.class);
		newsManager = context.getBean(NewsManager.class);
		commentManager = context.getBean(CommentManager.class);
	}

	public static void main(String[] args) {
		// authorManager.saveAuthors(AuthorsStorage.getAuthors());
		// authorManager.fetchSavedAuthors().forEach(System.out::println);
		// authorManager.fetchAuthorsWithNewsNumber();
		// tagManager.fetchAll().forEach(System.out::println);
		// newsManager.addTagsAndAuthorsToNews();
		// commentManager.addCommentsToAllNews();
		// tagManager.addTags();
		// newsManager.saveNews(NewsFactory.generateNewsList(9217));
		// newsManager.addTagsAndAuthorsToNews();
	}
}
