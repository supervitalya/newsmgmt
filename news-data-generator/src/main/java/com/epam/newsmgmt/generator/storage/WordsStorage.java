package com.epam.newsmgmt.generator.storage;

public class WordsStorage {
	public static final String[] SIMPLE_VERBS = { "eats", "dreams about", "goes crazy about", "sees", "thinks about",
			"fights with", "cooks", "hates" };
	public static final String[] CONTINUOUS_VERBS = { "is eating", "is searching for", "is dreaming about",
			"is catching", "is watching at", "is thinking about", "is cooking" };
	public static final String[] ADJECTIVES = { "Insane American", "Genious Belarussian", "Belarussian", "adorable",
			"crazy", "strong", "Russian", "Chineese", "American", "happy", "angry", "hungry", "furious", "insane",
			"friendly", "scared" };
	public static final String[] SUBJECT = { "Prime Minister", "President", "programmer", "child", "scientist",
			"photographer", "woman", "man", "football player", "formula 1  pilot", "weightlifter", "kid" };
	public static final String[] OBJECT = { "a sandwich", "potatoes", "a car", "a cake", "birds", "a tomato", "a cow",
			"Vasiliy Berezytskiy", "a helicopter" };
	public static final String[] SIMPLE_TIME = { "every day", "in the evenings", "on Mondays", "on Tuesdays",
			"every week", "every summer" };
	public static final String[] CONTINUOUS_TIME = { "today", "right now", "this year", "this month" };
	public static final String[] PLACE = { "in Belarus", "in America", "in Sahara Desert", "in Minsk",
			"on Times Square", "in Syberia", "in Mexico", "in Russia", "in Moscow", "in Chelyabinsk", "in Hong Kong" };

	public static final double CONTINUOUS_TITLE_CHANCE = 0.5;

}
