package com.epam.newsmgmt.generator.data;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.dto.AuthorDTO;

@Component
public class AuthorManager {
	@Autowired
	private AuthorDao authorDao;

	public Author[] saveAuthors(Author[] authors) {
		for (Author a : authors) {
			authorDao.insert(a);
		}
		return authors;
	}

	public List<Author> fetchSavedAuthors() {
		return authorDao.fetchAll();
	}

	public void fetchAuthorsWithNewsNumber() {
		List<AuthorDTO> authorWithNewsNumbers = authorDao.fetchAuthorsWithNewsNumber();
	}
}
