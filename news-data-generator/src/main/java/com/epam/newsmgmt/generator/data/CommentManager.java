package com.epam.newsmgmt.generator.data;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmgmt.domain.Comment;
import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.generator.factory.CommentsFactory;

@Transactional
@Component
public class CommentManager {

	@Autowired
	private SessionFactory factory;
	
	private static final int NEWS_NUMBER = 11000;
	private static final int BATCH_SIZE = 50;
	
	public void addCommentsToAllNews() {
		Session session = factory.getCurrentSession();

		for (int i = 0; i < NEWS_NUMBER; i += BATCH_SIZE) {
			List<News> batchNews = session.createCriteria(News.class).setFirstResult(i).setMaxResults(BATCH_SIZE).list();
			for (News n : batchNews) {
				List<Comment> comments = CommentsFactory.buildCommentsList(n.getId());
				for (Comment c : comments) {
					session.persist(c);
				}
				n.setComments(comments);
			}
			session.flush();
			session.clear();
		}
	}

}
