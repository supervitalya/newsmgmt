package com.epam.newsmgmt.generator.factory;

import java.util.Random;

import com.epam.newsmgmt.generator.storage.CharacterStorage;

public class TextGenerator {
	private static final int SENTENCE_MIN_LENGTH = 4;
	private static final int SENTENCE_MAX_LENGTH = 10;
	private static final double MIDDLE_PUNCT_CHANCE = 0.2;
	private static final String SPACE = " ";

	public static String generateText(int sentencesNumber) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < sentencesNumber; i++) {
			int sentenceLength = defineSentenceLength();
			builder.append(generateSentence(sentenceLength));
			builder.append(SPACE);
		}
		return builder.toString();
	}

	private static String generateSentence(int sentenceLength) {
		StringBuilder sentenceBuilder = new StringBuilder();
		for (int i = 0; i < sentenceLength - 1; i++) {
			sentenceBuilder.append(WordsGenerator.generateWord());
			if (Math.random() < MIDDLE_PUNCT_CHANCE) {
				sentenceBuilder.append(CharacterStorage.randomSentenceMiddlePunctuation());
			}
			sentenceBuilder.append(SPACE);
		}
		sentenceBuilder.append(WordsGenerator.generateWord());
		sentenceBuilder.append(CharacterStorage.randomSentenceEndPunctuation());
		String upperCaseFirstChar = sentenceBuilder.subSequence(0, 1).toString().toUpperCase();
		sentenceBuilder.replace(0, 1, upperCaseFirstChar);
		return sentenceBuilder.toString();
	}

	private static int defineSentenceLength() {
		return SENTENCE_MIN_LENGTH + new Random().nextInt(SENTENCE_MAX_LENGTH - SENTENCE_MIN_LENGTH);
	}
}
