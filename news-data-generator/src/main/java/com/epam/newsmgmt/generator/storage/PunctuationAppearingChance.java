package com.epam.newsmgmt.generator.storage;

public enum PunctuationAppearingChance {
	COMMA(0.8), COLON(0.1), SEMICOLON(0.1), POINT(0.9), EXCLAMATION_MARK(0.1);

	private double chance;

	private PunctuationAppearingChance(double chance) {
		this.chance = chance;
	}

	public double getChance() {
		return chance;
	}
}
