package com.epam.newsmgmt.generator.factory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.epam.newsmgmt.domain.Comment;

public class CommentsFactory {
	private static final int MIN_COMMENTS_NUMBER = 3;
	private static final int MAX_COMMENTS_NUMBER = 20;

	public static List<Comment> buildCommentsList(UUID newsId) {
		int number = MIN_COMMENTS_NUMBER + new Random().nextInt(MAX_COMMENTS_NUMBER - MIN_COMMENTS_NUMBER);
		List<Comment> comments = new ArrayList<>(number);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		for (int i = 0; i < number; i++) {
			comments.add(new Comment(null, TextGenerator.generateText(1), timestamp, newsId));
		}
		return comments;
	}
}
