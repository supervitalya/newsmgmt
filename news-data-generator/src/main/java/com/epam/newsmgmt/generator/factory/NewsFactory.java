package com.epam.newsmgmt.generator.factory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.epam.newsmgmt.domain.News;

public class NewsFactory {
	private static final int SENTENCES_IN_SHORT_TEXT = 1;
	private static final int SENTENCES_IN_FULL_TEXT = 10;

	public static List<News> generateNewsList(int number) {
		List<News> newsList = new ArrayList<>(number);
		for (int i = 0; i < number; i++) {
			newsList.add(buildNews());
		}
		return newsList;
	}

	private static News buildNews() {
		News news = new News();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		news.setCreationDate(timestamp);
		news.setModificationDate(timestamp);
		news.setTitle(HeadlineGenerator.buildNewsHeadline());
		news.setShortText(TextGenerator.generateText(SENTENCES_IN_SHORT_TEXT));
		news.setFullText(TextGenerator.generateText(SENTENCES_IN_FULL_TEXT));
		return news;
	}

}
