package com.epam.newsmgmt.generator.storage;

import java.util.Random;

public class TagsStorage {
	private static final String[] TAGS = { "IT", "art", "sport", "travelling", "people", "health", "culture",
			"economics", "politics", "science", "photo" };

	public static String getRandomTag() {
		Random random = new Random();
		return TAGS[random.nextInt(TAGS.length)];
	}
}
