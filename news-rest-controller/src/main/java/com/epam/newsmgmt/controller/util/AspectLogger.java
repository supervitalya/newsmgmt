package com.epam.newsmgmt.controller.util;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
@Aspect
public class AspectLogger {

	private static final Logger LOG = Logger.getLogger(AspectLogger.class);

	@AfterThrowing(pointcut = "execution(* com.epam.newsmgmt.controller.*.*(..))", throwing = "e")
	public void actionsOnExceptionThrowing(JoinPoint joinPoint, Exception e) {
		Signature signature = joinPoint.getSignature();
		String className = signature.getDeclaringTypeName();
		String methodName = signature.getName();
		String arguments = Arrays.toString(joinPoint.getArgs());
		LOG.error("Exception occured in class: " + className + ",method: " + methodName + " with agruments: "
				+ arguments);
		LOG.error("Full exception message: " + e);
	}

	@Around("execution(* com.epam.newsmgmt.controller.*.*(..))")
	public Object logTimeMethod(ProceedingJoinPoint joinPoint) throws Throwable {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Object retValue = joinPoint.proceed();
		stopWatch.stop();
		Signature signature = joinPoint.getSignature();
		LOG.info("Method " + signature.getDeclaringTypeName() + "." + signature.getName() + " finished in "
				+ stopWatch.getLastTaskTimeMillis() + " milliseconds.");
		return retValue;
	}
}
