package com.epam.newsmgmt.controller.exception;

import java.util.List;

import org.springframework.validation.FieldError;

public class IllegalRequestException extends RuntimeException {

	private List<FieldError> errors;

	public IllegalRequestException() {
		super();
	}

	public IllegalRequestException(List<FieldError> errors) {
		super();
		this.errors = errors;
	}

	public IllegalRequestException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public IllegalRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalRequestException(String message) {
		super(message);
	}

	public IllegalRequestException(Throwable cause) {
		super(cause);
	}

	public List<FieldError> getErrors() {
		return errors;
	}

	public void setErrors(List<FieldError> errors) {
		this.errors = errors;
	}

}
