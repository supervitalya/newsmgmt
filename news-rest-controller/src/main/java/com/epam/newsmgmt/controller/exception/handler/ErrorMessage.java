package com.epam.newsmgmt.controller.exception.handler;

public class ErrorMessage {
	private String message;

	public ErrorMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
