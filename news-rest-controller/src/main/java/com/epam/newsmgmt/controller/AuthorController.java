package com.epam.newsmgmt.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.newsmgmt.controller.exception.IllegalRequestException;
import com.epam.newsmgmt.dto.AuthorDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.service.AuthorService;

@RestController
@CrossOrigin
@RequestMapping(value = "/authors")
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "/{authorId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public AuthorDto loadAuthor(@PathVariable UUID authorId) {
		return authorService.getAuthorById(authorId);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<AuthorDto> loadAllAuthors() {
		return authorService.getAllAuthors();
	}

	@RequestMapping(params = { "number", "page" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<AuthorDto> loadAllAuthors(@ModelAttribute @Valid Paging paging) {
		return authorService.getAllAuthors(paging);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<AuthorDto> saveAuthor(@RequestBody @Valid AuthorDto authorDto, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		AuthorDto savedAuthor = authorService.saveAuthor(authorDto);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedAuthor.getId()).toUri());
		return new ResponseEntity<>(savedAuthor, httpHeaders, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/{authorId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateAuthor(@RequestBody @Valid AuthorDto authorDto, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		authorService.updateAuthor(authorDto);
	}

	@RequestMapping(value = "/{authorId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public AuthorDto removeAuthor(@PathVariable UUID authorId) {
		return authorService.removeAuthorById(authorId);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public long loadAllAuthorsNumber() {
		return authorService.getAllAuthorsNumber();
	}
}
