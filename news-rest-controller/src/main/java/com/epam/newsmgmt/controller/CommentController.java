package com.epam.newsmgmt.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.newsmgmt.controller.exception.IllegalRequestException;
import com.epam.newsmgmt.dto.CommentDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.service.CommentService;

@CrossOrigin
@RestController
public class CommentController {

	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/comments", method = RequestMethod.POST)
	public ResponseEntity<CommentDto> saveComment(@RequestBody @Valid CommentDto commentDto, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		CommentDto savedComment = commentService.saveComment(commentDto);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedComment.getId()).toUri());
		return new ResponseEntity<>(savedComment, httpHeaders, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/comments/{commentId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateComment(@RequestBody @Valid CommentDto commentDto, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		commentService.updateComment(commentDto);
	}

	@RequestMapping(value = "/comments/{commentId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public CommentDto removeComment(@PathVariable UUID commentId) {
		return commentService.removeCommentById(commentId);
	}

	@RequestMapping(value = "/news/{newsId}/comments", params = { "number", "page" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<CommentDto> loadCommentsByNewsId(@PathVariable UUID newsId, @ModelAttribute @Valid Paging paging) {
		return commentService.getAllCommentsByNewsId(newsId, paging);
	}

	@RequestMapping(value = "/news/{newsId}/comments/count", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public long loadAllNewsNumber(@PathVariable UUID newsId) {
		return commentService.getAllCommentsNumber(newsId);
	}
}
