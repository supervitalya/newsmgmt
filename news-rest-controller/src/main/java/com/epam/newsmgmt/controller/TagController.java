package com.epam.newsmgmt.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.newsmgmt.controller.exception.IllegalRequestException;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.TagDto;
import com.epam.newsmgmt.service.TagService;

@CrossOrigin
@RestController
@RequestMapping(value = "/tags")
public class TagController {

	@Autowired
	private TagService tagService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<TagDto> loadAllTags() {
		return tagService.getAllTags();
	}

	@RequestMapping(params = { "number", "page" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<TagDto> loadAllTags(@ModelAttribute @Valid Paging paging) {
		return tagService.getAllTags(paging);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<TagDto> saveTag(@RequestBody @Valid TagDto tag, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		TagDto savedTag = tagService.saveTag(tag);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path("/{tag}")
				.buildAndExpand(savedTag.getTag()).toUri());
		return new ResponseEntity<>(savedTag, httpHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{tagId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateTag(@PathVariable String tagId, @RequestBody @Valid TagDto newTag, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		tagService.updateTag(new TagDto(tagId), newTag);
	}

	@RequestMapping(value = "/{tagId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public TagDto removeTag(@PathVariable String tagId) {
		return tagService.removeTag(new TagDto(tagId));
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public long loadAllTagsNumber() {
		return tagService.getAllTagsNumber();
	}
}
