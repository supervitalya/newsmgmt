package com.epam.newsmgmt.controller.exception.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.newsmgmt.controller.exception.IllegalRequestException;

@ControllerAdvice
public class DefaultExceptionHandler {
	private static final Logger LOG = Logger.getLogger(DefaultExceptionHandler.class);

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ErrorMessage> handleConflictException(DataIntegrityViolationException e) {
		/*
		 * The exception occurs when you're trying to remove entity which other
		 * entity depends on or to insert duplicate value. Status 409.
		 */
		String message = e.getRootCause().getMessage();
		ErrorMessage errorMessage;
		if (message.contains("child record found")) {
			errorMessage = new ErrorMessage("Entity cannot be removed. Child record found.");
		} else if (message.contains("unique constraint")) {
			errorMessage = new ErrorMessage("Cannot add duplicate entity.");
		} else {
			errorMessage = new ErrorMessage("Incorrect data. Please check your request.");
		}
		return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorMessage> handleConstraintException(ConstraintViolationException e) {
		/*
		 * Status 409.
		 */
		LOG.error(e);
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Incorrect data. Please check your request."),
				HttpStatus.CONFLICT);
	}

	@ExceptionHandler(BindException.class)
	public ResponseEntity<ErrorMessage> handleIllegalArgument(BindException e) {
		/*
		 * Exception occurs when paging parameters are incorrect. Status 400.
		 */
		LOG.error(e);
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Request parameters are not valid."),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<ErrorMessage> handleIllegalArgument(IllegalArgumentException e) {
		/*
		 * Exception occurs when passed id is null. Status 400.
		 */
		LOG.error(e);
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Incorrect request."), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HibernateOptimisticLockingFailureException.class)
	public ResponseEntity<ErrorMessage> handleOptimisticLockingException(HibernateOptimisticLockingFailureException e) {
		/*
		 * Optimistic lock exception handling. Status code 423.
		 */
		LOG.error(e);
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Entity is locked. Try to update it later."),
				HttpStatus.LOCKED);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorMessage> handleValidationException(MethodArgumentNotValidException e) {
		/*
		 * Validation exceptions handling. Status code 422.
		 */
		LOG.error(e);
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Request parameters are not valid."),
				HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(IllegalRequestException.class)
	public ResponseEntity<List<String>> handleValidation(IllegalRequestException e) {
		/*
		 * Validation exceptions handling. Status code 422.
		 */

		List<String> errors = new ArrayList<>();
		e.getErrors().forEach(er -> errors.add(String.format("Incorrect value for field %s : '%s'. %s.", er.getField(),
				er.getRejectedValue(), er.getDefaultMessage())));
		return new ResponseEntity<List<String>>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorMessage> handleOthersException(Exception e) {
		/* Handles all other exceptions. Status code 500. */
		LOG.error(e);
		return new ResponseEntity<ErrorMessage>(new ErrorMessage("Internal server error."),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
