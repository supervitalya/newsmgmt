package com.epam.newsmgmt.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.newsmgmt.controller.exception.IllegalRequestException;
import com.epam.newsmgmt.dto.NewsDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.TagDto;
import com.epam.newsmgmt.service.CommentService;
import com.epam.newsmgmt.service.NewsService;

@CrossOrigin
@RestController
@RequestMapping("/news")
public class NewsController {

	@Autowired
	private NewsService newsService;

	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public NewsDto loadNews(@PathVariable UUID newsId) {
		return newsService.getNewsById(newsId);
	}

	@RequestMapping(params = { "number", "page" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<NewsDto> loadAll(@ModelAttribute @Valid Paging paging) {
		return newsService.getAllNews(paging);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<NewsDto> saveNews(@RequestBody @Valid NewsDto newsDto, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		NewsDto savedNews = newsService.saveNews(newsDto);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedNews.getId()).toUri());
		return new ResponseEntity<>(savedNews, httpHeaders, HttpStatus.CREATED);
	}

	@Transactional
	@RequestMapping(value = "/{newsId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateNews(@RequestBody @Valid NewsDto newsDto, BindingResult result) {
		if (result.hasErrors()) {
			throw new IllegalRequestException(result.getFieldErrors());
		}
		newsDto.setComments(commentService.getAllCommentsByNewsId(newsDto.getId()));
		newsService.updateNews(newsDto);
	}

	@RequestMapping(value = "/{newsId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public NewsDto removeNews(@PathVariable UUID newsId) {
		return newsService.removeNewsById(newsId);
	}

	@RequestMapping(params = { "number", "page", "authorId" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<NewsDto> loadNewsByAuthorId(@ModelAttribute @Valid Paging paging, @RequestParam UUID authorId) {
		return newsService.getAllNewsByAuthorId(authorId, paging);
	}

	@RequestMapping(params = { "number", "page", "tag" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<NewsDto> loadNewsByTag(@ModelAttribute @Valid Paging paging, @RequestParam @Valid TagDto tag) {
		return newsService.getAllNewsByTag(tag, paging);
	}

	@RequestMapping(params = { "number", "page", "topCommented" }, method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<NewsDto> loadNewsSortedByComments(@ModelAttribute @Valid Paging paging,
			@RequestParam int topCommented) {
		return newsService.getAllNewsSortedByComments(topCommented, paging);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public long loadAllNewsNumber() {
		return newsService.getAllNewsNumber();
	}

	@RequestMapping(value = "/count", params = "authorId", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public long loadNewsNumberByAuthorId(@RequestParam UUID authorId) {
		return newsService.getNewsNumberByAuthorId(authorId);
	}

	@RequestMapping(value = "/count", params = "tag", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public long loadAllNewsNumberByTag(@RequestParam String tag) {
		return newsService.getNewsNumberByTag(new TagDto(tag));
	}

}
