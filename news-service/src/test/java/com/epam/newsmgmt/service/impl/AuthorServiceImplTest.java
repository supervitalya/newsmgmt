package com.epam.newsmgmt.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.dto.AuthorDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.converter.Converter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/service-context.xml" })
public class AuthorServiceImplTest {
	@Autowired
	private Converter<Author, AuthorDto> authorConverter;

	@Mock
	private AuthorDao authorDao;

	@InjectMocks
	private AuthorServiceImpl authorService;

	private List<Author> authorsDefaultList;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		authorsDefaultList = generateAuthorsList();
		authorService.setAuthorConverter(authorConverter);
	}

	@Test
	public void insertAuthorTest() {
		AuthorDto author = new AuthorDto(null, "John", "Lennon");
		AuthorDto insertedExpected = new AuthorDto(UUID.randomUUID(), "John", "Lennon");
		Author author2 = authorConverter.fromDto(author);
		Mockito.when(authorDao.insert(authorConverter.fromDto(author)))
				.thenReturn(authorConverter.fromDto(insertedExpected));

		AuthorDto insertedActual = authorService.saveAuthor(author);
		Mockito.verify(authorDao, times(1)).insert(authorConverter.fromDto(author));
		assertEquals(insertedExpected.getId(), insertedActual.getId());
	}

	@Test
	public void updateAuthorTest() {
		Author author = authorsDefaultList.get(0);
		authorService.updateAuthor(authorConverter.toDto(author));
		Mockito.verify(authorDao, times(1)).update(author);
	}

	@Test
	public void fetchAllAuthorsTest() {
		Mockito.when(authorDao.fetchAll(0, 4)).thenReturn(authorsDefaultList);
		Paging paging = new Paging();
		paging.setNumber(4);
		paging.setPage(1);
		List<Author> actualList = authorService.getAllAuthors(paging).stream()
				.map(author -> authorConverter.fromDto(author)).collect(Collectors.toList());
		Mockito.verify(authorDao, times(1)).fetchAll(0, 4);
		assertEquals(authorsDefaultList, actualList);
	}

	@Test
	public void removeAuthorTest() {
		UUID authorId = UUID.randomUUID();
		Author expected = authorsDefaultList.get(2);

		Mockito.when(authorDao.remove(authorId)).thenReturn(expected);
		Author actual = authorConverter.fromDto(authorService.removeAuthorById(authorId));
		assertEquals(expected, actual);
	}

	private List<Author> generateAuthorsList() {
		List<Author> authorsList = new ArrayList<Author>() {
			{
				add(new Author(UUID.randomUUID(), "John", "Lennon"));
				add(new Author(UUID.randomUUID(), "Jimmy", "Page"));
				add(new Author(UUID.randomUUID(), "Ozzy", "Osbourne"));
				add(new Author(UUID.randomUUID(), "Jimmy", "Hendrix"));
			}
		};
		return authorsList;
	}
}
