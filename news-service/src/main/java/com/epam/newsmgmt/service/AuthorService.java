package com.epam.newsmgmt.service;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.dto.AuthorDto;
import com.epam.newsmgmt.dto.Paging;

/**
 * Performs operations with Author entities. All methods works with AuthorDto.
 * Author entities are converted to AuthorDto's using converters.
 * 
 * @see com.epam.newsmgmt.dto.converter.Converter
 * @author Vitali_Bliakharchuk
 *
 */
public interface AuthorService {
	/**
	 * Retrieves AuthorDto for Author with specified id.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @return
	 */
	AuthorDto getAuthorById(UUID authorId);

	/**
	 * Retrieves list of AuthorDto starting from specified position with
	 * specified amount.
	 * 
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of AuthorDto entities
	 */
	List<AuthorDto> getAllAuthors(Paging paging);

	/**
	 * Retrieves list of all AuthorDtos.
	 * 
	 * @return list of AuthorDto entities.
	 */
	List<AuthorDto> getAllAuthors();

	/**
	 * Retrieves number of all Author entities.
	 * 
	 * @return number of all Author entities
	 */
	long getAllAuthorsNumber();

	/**
	 * Adds Author entity.
	 * 
	 * @param author
	 *            DTO
	 * @return AuthorDto with generated id
	 */
	AuthorDto saveAuthor(AuthorDto author);

	/**
	 * Updates Author entite
	 * 
	 * @param author
	 *            DTO
	 */
	void updateAuthor(AuthorDto author);

	/**
	 * Removes Author entity by it's id.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @return AuthorDto DTO containing removed entity
	 */
	AuthorDto removeAuthorById(UUID authorId);

}
