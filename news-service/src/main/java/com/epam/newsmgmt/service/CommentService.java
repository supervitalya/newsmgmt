package com.epam.newsmgmt.service;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.dto.CommentDto;
import com.epam.newsmgmt.dto.Paging;

/**
 * Performs operations with Comment entities. All methods works with CommentDto.
 * Comment entities are converted to CommentDto's using converters.
 * 
 * @author Vitali_Bliakharchuk
 *
 */
public interface CommentService {
	/**
	 * Removes Comment entity by it's id. Returns DTO with generated id.
	 * 
	 * @param commentId
	 *            Comment entity id.
	 * @return DTO with generated id
	 */
	CommentDto removeCommentById(UUID commentId);

	/**
	 * Updates Comment entity.
	 * 
	 * @param comment
	 *            DTO
	 */
	void updateComment(CommentDto comment);

	/**
	 * Adds Comment entity using DTO.
	 * 
	 * @param comment
	 *            DTO
	 * @return DTO with generated ID
	 */
	CommentDto saveComment(CommentDto comment);

	/**
	 * Retrieves all Comment entities related to News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of CommentDto
	 */
	List<CommentDto> getAllCommentsByNewsId(UUID newsId, Paging paging);

	/**
	 * Retrieves all Comment entities related to News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return list of CommentDto
	 */
	List<CommentDto> getAllCommentsByNewsId(UUID newsId);

	/**
	 * Retrieves number of all Comment entities related to News entity with
	 * specified id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @return number of Comment entities
	 */
	long getAllCommentsNumber(UUID newsId);
}
