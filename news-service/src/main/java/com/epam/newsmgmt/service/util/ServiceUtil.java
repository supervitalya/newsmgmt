package com.epam.newsmgmt.service.util;

public class ServiceUtil {
	/**
	 * Defines news number on a single page. If the page is not last it returns
	 * number argument. But if we fetch the last page, for example, for top 10
	 * news and we have 3 news on single page we must define number news on last
	 * page as 1 which is modulo.
	 */
	public static int defineNewsNumberAtPage(int maxResults, int number, int page) {
		if (number == 1) {
			return number;
		}
		if (maxResults < number) {
			return maxResults;
		}

		return ((page * number) > maxResults) ? maxResults % number : number;
	}

	public static int defineStartPosition(int page, int number) {
		return (page - 1) * number;
	}
}
