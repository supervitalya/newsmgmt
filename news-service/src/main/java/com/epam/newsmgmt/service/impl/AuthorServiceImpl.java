package com.epam.newsmgmt.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.dto.AuthorDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.converter.Converter;
import com.epam.newsmgmt.service.AuthorService;
import com.epam.newsmgmt.service.util.ServiceUtil;

@Service
public class AuthorServiceImpl implements AuthorService {
	@Autowired
	private AuthorDao authorDao;

	@Autowired
	private Converter<Author, AuthorDto> authorConverter;

	public void setAuthorConverter(Converter<Author, AuthorDto> authorConverter) {
		this.authorConverter = authorConverter;
	}

	@Override
	public AuthorDto getAuthorById(UUID authorId) {
		return authorConverter.toDto(authorDao.fetchById(authorId));
	}

	@Override
	public List<AuthorDto> getAllAuthors(Paging paging) {
		int startPosition = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<Author> authorsList = authorDao.fetchAll(startPosition, paging.getNumber());
		return authorsList.stream().map((author) -> authorConverter.toDto(author)).collect(Collectors.toList());
	}

	@Override
	public AuthorDto saveAuthor(AuthorDto authorDto) {
		return authorConverter.toDto(authorDao.insert(authorConverter.fromDto(authorDto)));
	}

	@Override
	public void updateAuthor(AuthorDto authorDto) {
		authorDao.update(authorConverter.fromDto(authorDto));
	}

	@Override
	public AuthorDto removeAuthorById(UUID authorId) {
		return authorConverter.toDto(authorDao.remove(authorId));
	}

	@Override
	public List<AuthorDto> getAllAuthors() {
		List<Author> authorsList = authorDao.fetchAll();
		return authorsList.stream().map((author) -> authorConverter.toDto(author)).collect(Collectors.toList());
	}

	@Override
	public long getAllAuthorsNumber() {
		return authorDao.fetchAllAuthorsNumber();
	}

}
