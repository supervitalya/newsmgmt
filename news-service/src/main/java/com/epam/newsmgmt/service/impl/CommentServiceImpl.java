package com.epam.newsmgmt.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmgmt.dao.CommentDao;
import com.epam.newsmgmt.domain.Comment;
import com.epam.newsmgmt.dto.CommentDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.converter.Converter;
import com.epam.newsmgmt.service.CommentService;
import com.epam.newsmgmt.service.util.ServiceUtil;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	private CommentDao commentDao;
	@Autowired
	private Converter<Comment, CommentDto> commentConverter;

	public void setCommentConverter(Converter<Comment, CommentDto> commentConverter) {
		this.commentConverter = commentConverter;
	}

	@Override
	public CommentDto removeCommentById(UUID commentId) {
		return commentConverter.toDto(commentDao.remove(commentId));
	}

	@Override
	public void updateComment(CommentDto commentDto) {
		commentDao.update(commentConverter.fromDto(commentDto));
	}

	@Override
	public CommentDto saveComment(CommentDto commentDto) {
		return commentConverter.toDto(commentDao.insert(commentConverter.fromDto(commentDto)));
	}

	@Override
	public List<CommentDto> getAllCommentsByNewsId(UUID newsId, Paging paging) {
		int startPosition = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<Comment> commentsList = commentDao.fetchByNewsId(newsId, startPosition, paging.getNumber());
		return commentsList.stream().map((comment) -> commentConverter.toDto(comment)).collect(Collectors.toList());
	}

	@Override
	public List<CommentDto> getAllCommentsByNewsId(UUID newsId) {
		List<Comment> commentsList = commentDao.fetchByNewsId(newsId);
		return commentsList.stream().map((comment) -> commentConverter.toDto(comment)).collect(Collectors.toList());
	}

	@Override
	public long getAllCommentsNumber(UUID newsId) {
		return commentDao.fetchAllCommentsNumberByNewsId(newsId);
	}

}
