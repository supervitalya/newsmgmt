package com.epam.newsmgmt.service;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.TagDto;

/**
 * Performs operations with Tag entities. All methods works with AuthorDto. Tag
 * entities are converted to TagDto's using converters.
 * 
 * @see com.epam.newsmgmt.dto.converter.Converter
 * @author Vitali_Bliakharchuk
 *
 */
public interface TagService {
	/**
	 * Add Tag entity. Returns DTO containing generated id.
	 * 
	 * @param tag
	 *            DTO
	 * @return TagDTO containing generated id
	 */
	TagDto saveTag(TagDto tag);

	/**
	 * Updates Tag entity.
	 * 
	 * @param oldTag
	 *            TagDTO containing old tag
	 * @param newTag
	 *            TagDTO containing new tag
	 */
	void updateTag(TagDto oldTag, TagDto newTag);

	/**
	 * Retrieves list of TagDto with specified page number and maximal number of
	 * entities.
	 * 
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of TagDTO
	 */
	List<TagDto> getAllTags(Paging paging);

	/**
	 * Retrieves list of TagDto entities.
	 * 
	 * @return list of TagDTO
	 */
	List<TagDto> getAllTags();

	/**
	 * Retrieves number of all Tag entities.
	 * 
	 * @return number of all Tag entities
	 */
	long getAllTagsNumber();

	/**
	 * Retrieves list of TagDto entities related to News entity with specified
	 * id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @return list of TagDto entities
	 */
	List<TagDto> getAllTagsByNewsId(UUID newsId);

	/**
	 * Removes Tag entity.
	 * 
	 * @param tag
	 *            TagDto entity
	 * @return TagDto containing removed Tag entity.
	 */
	TagDto removeTag(TagDto tag);
}
