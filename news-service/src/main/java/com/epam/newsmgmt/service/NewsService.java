package com.epam.newsmgmt.service;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.dto.NewsDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.TagDto;

/**
 * Performs operations with News entities. All methods works with NewsDto. News
 * entities are converted to NewsDto's using converters.
 * 
 * @see com.epam.newsmgmt.dto.converter.Converter
 * @author Vitali_Bliakharchuk
 *
 */
public interface NewsService {
	/**
	 * Retrieves NewsDto by News id.
	 * 
	 * @param newsId
	 *            News entity id
	 * @return NewsDto
	 */
	NewsDto getNewsById(UUID newsId);

	/**
	 * Updates News entity.
	 * 
	 * @param newsDto
	 *            DTO containing News entity
	 */
	void updateNews(NewsDto newsDto);

	/**
	 * Removes News entity with specified Id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return DTO containing removed News entity.
	 */
	NewsDto removeNewsById(UUID newsId);

	/**
	 * Adds News entity. Returns NewsDto containing generated id.
	 * 
	 * @param newsDto
	 * @return NewsDto containing generated id
	 */
	NewsDto saveNews(NewsDto newsDto);

	/**
	 * Retrieves all NewsDto entities from specified page number with specified
	 * maximal results number.
	 * 
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of NewsDto entities
	 */
	List<NewsDto> getAllNews(Paging paging);

	/**
	 * Retrieves number of all News entities.
	 * 
	 * @return number of all News entities
	 */
	long getAllNewsNumber();

	/**
	 * Retrieves number of all News entities related to Author entity with
	 * specified id from specified page number with specified maximal results
	 * number.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of NewsDto entities
	 */
	List<NewsDto> getAllNewsByAuthorId(UUID authorId, Paging paging);

	/**
	 * Retrieves number of News entities related to Author with specified id.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @return number of news.
	 */
	long getNewsNumberByAuthorId(UUID authorId);

	/**
	 * Retrieves News related to specified tag.
	 * 
	 * @param tagDto
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of NewsDto
	 */
	List<NewsDto> getAllNewsByTag(TagDto tagDto, Paging paging);

	/**
	 * Retrieves number of News entities related to Tag entity.
	 * 
	 * @param tagDto
	 * @return number of News
	 */
	long getNewsNumberByTag(TagDto tagDto);

	/**
	 * Retrieves list of all News entities sorted by number of comments and
	 * paginated.
	 * 
	 * @param maxResults
	 *            maximal number of news
	 * @param paging
	 *            objects includes fetching start position and maximal number of
	 *            entities
	 * @return list of News entities sorted by comments number
	 */
	List<NewsDto> getAllNewsSortedByComments(int maxResults, Paging paging);

}
