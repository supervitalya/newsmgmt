package com.epam.newsmgmt.service.impl;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmgmt.dao.TagDao;
import com.epam.newsmgmt.domain.Tag;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.TagDto;
import com.epam.newsmgmt.dto.converter.Converter;
import com.epam.newsmgmt.service.TagService;
import com.epam.newsmgmt.service.util.ServiceUtil;

@Service
public class TagServiceImpl implements TagService {
	@Autowired
	private TagDao tagDao;

	@Autowired
	private Converter<Tag, TagDto> tagConverter;

	public void setTagConverter(Converter<Tag, TagDto> tagConverter) {
		this.tagConverter = tagConverter;
	}

	@Override
	public TagDto saveTag(TagDto tagDto) {
		return tagConverter.toDto(tagDao.insert(tagConverter.fromDto(tagDto)));
	}

	@Override
	public void updateTag(TagDto oldTagDto, TagDto newTagDto) {
		tagDao.update(tagConverter.fromDto(oldTagDto), tagConverter.fromDto(newTagDto));
	}

	@Override
	public List<TagDto> getAllTags(Paging paging) {
		int startPosition = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<Tag> tagsList = tagDao.fetchAll(startPosition, paging.getNumber());
		return tagsList.stream().map((tag) -> tagConverter.toDto(tag)).collect(Collectors.toList());
	}

	@Override
	public List<TagDto> getAllTagsByNewsId(UUID newsId) {
		List<Tag> tagsList = tagDao.fetchByNewsId(newsId);
		return tagsList.stream().map((tag) -> tagConverter.toDto(tag)).collect(Collectors.toList());
	}

	@Override
	public TagDto removeTag(TagDto tagDto) {
		return tagConverter.toDto(tagDao.remove(tagConverter.fromDto(tagDto)));
	}

	@Override
	public List<TagDto> getAllTags() {
		return tagDao.fetchAll().stream().map((tag) -> tagConverter.toDto(tag)).collect(Collectors.toList());
	}

	@Override
	public long getAllTagsNumber() {
		return tagDao.fetchAllTagsNumber();
	}

}
