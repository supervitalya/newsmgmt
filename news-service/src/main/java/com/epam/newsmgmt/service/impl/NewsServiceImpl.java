package com.epam.newsmgmt.service.impl;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmgmt.dao.NewsDao;
import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.domain.Tag;
import com.epam.newsmgmt.dto.NewsDto;
import com.epam.newsmgmt.dto.Paging;
import com.epam.newsmgmt.dto.TagDto;
import com.epam.newsmgmt.dto.converter.Converter;
import com.epam.newsmgmt.service.NewsService;
import com.epam.newsmgmt.service.util.ServiceUtil;

@Service
public class NewsServiceImpl implements NewsService {
	@Autowired
	private NewsDao newsDao;
	@Autowired
	private Converter<News, NewsDto> newsConverter;
	@Autowired
	private Converter<Tag, TagDto> tagConverter;

	public void setNewsConverter(Converter<News, NewsDto> newsConverter) {
		this.newsConverter = newsConverter;
	}

	public void setTagConverter(Converter<Tag, TagDto> tagConverter) {
		this.tagConverter = tagConverter;
	}

	@Override
	public NewsDto getNewsById(UUID newsId) {
		return newsConverter.toDto(newsDao.fetchById(newsId));
	}

	@Override
	public void updateNews(NewsDto newsDto) {
		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
		newsDto.setModificationDate(currentDate);
		newsDao.update(newsConverter.fromDto(newsDto));
	}

	@Override
	public NewsDto removeNewsById(UUID newsId) {
		return newsConverter.toDto(newsDao.remove(newsId));
	}

	@Override
	public NewsDto saveNews(NewsDto newsDto) {
		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
		newsDto.setCreationDate(currentDate);
		newsDto.setModificationDate(currentDate);
		return newsConverter.toDto(newsDao.insert(newsConverter.fromDto(newsDto)));
	}

	@Override
	public List<NewsDto> getAllNews(Paging paging) {
		int startFrom = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<News> newsList = newsDao.fetchAll(startFrom, paging.getNumber());
		return newsList.stream().map((news) -> newsConverter.toDto(news)).collect(Collectors.toList());
	}

	@Override
	public List<NewsDto> getAllNewsByAuthorId(UUID authorId, Paging paging) {
		int startFrom = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<News> newsList = newsDao.fetchByAuthorId(authorId, startFrom, paging.getNumber());
		return newsList.stream().map((news) -> newsConverter.toDto(news)).collect(Collectors.toList());
	}

	@Override
	public List<NewsDto> getAllNewsByTag(TagDto tagDto, Paging paging) {
		int startFrom = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<News> newsList = newsDao.fetchByTag(tagConverter.fromDto(tagDto), startFrom, paging.getNumber());
		return newsList.stream().map((news) -> newsConverter.toDto(news)).collect(Collectors.toList());
	}

	@Override
	public List<NewsDto> getAllNewsSortedByComments(int maxResults, Paging paging) {
		int newsNumber = ServiceUtil.defineNewsNumberAtPage(maxResults, paging.getNumber(), paging.getPage());
		if (newsNumber == 0) {
			return Collections.emptyList();
		}
		int startFrom = ServiceUtil.defineStartPosition(paging.getPage(), paging.getNumber());
		List<News> newsList = newsDao.fetchSortedByComments(startFrom, newsNumber);
		return newsList.stream().map((news) -> newsConverter.toDto(news)).collect(Collectors.toList());
	}

	@Override
	public long getAllNewsNumber() {
		return newsDao.fetchCountAll();
	}

	@Override
	public long getNewsNumberByAuthorId(UUID authorId) {
		return newsDao.fetchCountByAuthorID(authorId);
	}

	@Override
	public long getNewsNumberByTag(TagDto tagDto) {
		return newsDao.fetchCountByTag(tagConverter.fromDto(tagDto));
	}

}
