angular.
module('newsView')
.component('newsView', {
	templateUrl: 'components/news/view/news-view-template.html',
	controller: ['CommentService','NewsService','$routeParams','$location', '$modal', '$http','DEFAULT_URL',
	function NewsViewController(CommentService, NewsService, $routeParams, $location, $modal, $http, DEFAULT_URL){
		var that = this;
		var newsId = $routeParams.id;
	
		that.news = {comments:[],authors:[],tags:[]};

		if(!that.number) {
			that.number = 5;
		}
		if(!that.page) {
			that.page = 1;
		}
		
		this.updateComments = function(){
			that.errorMessage=null;
			$http.get(DEFAULT_URL+'/news/'+newsId+'/comments?number='+that.number+'&page='+that.page).then(
				function(result){
					that.comments = result.data;
				},errorHandler);
			$http.get(DEFAULT_URL+'/news/'+newsId+'/comments/count').then(
				function(result){
					that.commentsNumber = result.data;
				},errorHandler);
		}

		var fetchedNews = NewsService.get({id:newsId},
			function(){
				that.news = fetchedNews;
				that.updateComments();
			},errorHandler);
		
		
		this.addComment = function(){
			var currentTimestamp = new Date().getTime();
			var comment = {
				commentText: that.newComment,
				creationDate: currentTimestamp,
				newsId: that.news.id
			};
			CommentService.save(comment, 
				function(){
					that.updateComments();
					that.newComment = null;
				},errorHandler);
		}

		this.deleteComment = function(comment){
				var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/accept-dialog.template.html',
				controller: 'AcceptDialogController',
				controllerAs: '$ctrl',resolve: {
					windowTitle: function(){
						return 'Delete comment';
					}
				}
			});
			dialog.result.then(
				function(accepted){
					if(accepted){
						CommentService.delete({commentId: comment.id},that.updateComments);
					}
				}
			);
		}
		
		var errorHandler = function (error){
			switch(error.status){
				case 422:
					that.errorMessage = 'Comment is invalid.';
					break;
				default:
					$location.path('/error/'+error.status);
			}
		}
	}
	]
});