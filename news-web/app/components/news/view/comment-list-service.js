angular.module('newsView')
.factory('CommentService',['$resource', 'DEFAULT_URL',function($resource, DEFAULT_URL){
	return $resource(DEFAULT_URL+'/comments/:commentId',
	 { commentId: '@id' },	{
		delete: {
			method: 'DELETE'
		}
	});
}]);
