angular.
module('newsList')
.component('newsList', {
	templateUrl: 'components/news/list/news-list-template.html',
	controller: ['NewsService','TagService','AuthorService','DEFAULT_URL','$location','$modal', '$http',
	function (NewsService, TagService, AuthorService, DEFAULT_URL, $location, $modal, $http){
		
		var that = this;
		
		if(!this.number) {
			this.number = 10;
		}
		if(!this.page) {
			this.page = 1;
		}
		this.newsNumberUrl = DEFAULT_URL+'/news/count';
		
		this.updatePage = function(){
			updateTagsDropdown();
			updateAuthorsDropdown();
			var query = {'number': that.number, 'page': that.page};
			if (that.searchBy) {
				query[that.searchBy] = that.searchingCriteria;
			}
			var fetchedNews = NewsService.query(query, function(){
				that.newsList = fetchedNews;
			},errorHandler);

			if(that.newsNumberUrl){
				$http.get(that.newsNumberUrl).then(function(result){
					that.newsNumber = result.data;
				}, errorHandler);
			}
			that.errorMessage = null;
			that.successMessage = null;
		}
		
		this.updatePage();

		function updateTagsDropdown(){
			var fetchedTags = TagService.query({},
				function(){
					that.tagsList = fetchedTags;
				},errorHandler);
		}

		function updateAuthorsDropdown(){
			var fetchedAuthors = AuthorService.query({},
				function(){
					that.authorsList = fetchedAuthors;
				},errorHandler);
		}

		this.changeNewsNumber = function(){
			that.page = 1;
			that.updatePage();
		}
		
		this.search = function (searchParameter, value){
			that.searchBy = searchParameter;
			that.page = 1;
			that.searchingCriteria = value;
			if (searchParameter === 'topCommented'){
				that.newsNumber = value;
				that.newsNumberUrl = false;
			}else{
				that.newsNumberUrl = DEFAULT_URL + '/news/count?'+searchParameter+'='+value;
			}
			that.updatePage();
		}

		this.deleteNews = function(newsId){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/accept-dialog.template.html',
				controller: 'AcceptDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Delete news';
					}
				}
			});
			dialog.result.then(function (accepted){
				acceptDelete(accepted, newsId);
			});
		}

		function acceptDelete(accepted, newsId){
			if(accepted){
				NewsService.delete({id:newsId}, that.updatePage,errorHandler);
			}
		}

		this.addNews = function(){
			var allAuthors = AuthorService.query(function(){},errorHandler);
			var allTags = TagService.query(function(){},errorHandler);
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/news-dialog.template.html',
				controller: 'NewsDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Add news:';
					},
				    tags: function () {
				        return allTags;
				    },
				    authors: function () {
				        return allAuthors;
				    },
				    news: function(){
				    	return null;
				    }
				}
			});
			dialog.result.then(function (news){
				if(news){
				acceptAdd(news);
				}
			});
		}

		var acceptAdd = function(news){
			NewsService.save(news, that.updatePage,errorHandler);
		}

		this.updateNews = function(news){
			var allAuthors = AuthorService.query(
				function(){
					removeDuplicatesFromAuthorsDropdown(allAuthors,news);
				},errorHandler);
			var allTags = TagService.query(
				function(){
					removeDuplicatesFromTagsDropdown(allTags,news);
				},errorHandler);
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/news-dialog.template.html',
				controller: 'NewsDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle:function(){
						return 'Update news:';
					},
				    tags: function () {
				        return allTags;
				    },
				    authors: function () {
				        return allAuthors;
				    },
				    news: function(){
				    	return news;
				    }
				}
			});
			dialog.result.then(function (news){
				if(news){
					acceptUpdate(news);
				}
			});
		}

		var acceptUpdate = function(news){
			NewsService.update(news, that.updatePage, errorHandler);
		}

		function removeDuplicatesFromAuthorsDropdown(authors, news){
			for(var i=0; i < news.authors.length; i++){
				for(var j=0; j < authors.length; j++){
					if (authors[j].id === news.authors[i].id){
						authors.splice(j, 1);
						j--;
					}
				}
			}
		}

		function removeDuplicatesFromTagsDropdown(tags, news){
			for(var i=0; i < news.tags.length;i++){
				for(var j=0; j < tags.length; j++){
					if (tags[j].tag === news.tags[i].tag){
						tags.splice(j, 1);
						j--;
					}
				}
			}
		}

		var errorHandler = function (error){
			console.log(error);
			switch(error.status){
				case 422:
					that.updatePage();
					that.errorMessage = error.data.message;
					break;
				case 423:
					that.updatePage();
					that.errorMessage = error.data.message;
					break;
				default:
					$location.path('/error/'+error.status);
			}
		}
	}]
});