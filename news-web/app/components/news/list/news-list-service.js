angular.
module('newsList')
	.factory('NewsService',	['$resource','DEFAULT_URL', function($resource, DEFAULT_URL){
		return $resource(DEFAULT_URL+'/news/:id', { id: '@id' }, {
		    update: {
		      method: 'PUT'
		    }
		},
		{
		    delete: {
		      method: 'DELETE'
		    }
		});
	}]);

