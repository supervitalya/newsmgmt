angular.module('newsList')
	.controller('NewsDialogController', ['$modalInstance','authors','tags', 'news','windowTitle',
		function ($modalInstance, authors, tags, news, windowTitle) {
			
	var that = this;
	this.authorsList = authors;
	this.tagsList = tags;
	this.authors=[];
	this.tags=[];
	this.windowTitle = windowTitle;

	if(news === null){
		this.news ={authors:[],tags:[]};
	}else{
		this.title = news.title;
		this.shortText = news.shortText;
		this.fullText = news.fullText;
		this.authors = news.authors.slice();
		this.tags = news.tags.slice();
		this.news = news;
	}
	
	this.cancel = function() {
		$modalInstance.close(false);
	}

	this.save = function(){
		that.news.title = that.title;
		that.news.shortText = that.shortText;
		that.news.fullText = that.fullText;
		that.news.authors = that.authors;
		that.news.tags = that.tags;
		$modalInstance.close(that.news);
	}
	
	this.addAuthor = function(author){
		that.authors.push(author);
		var authorIndex = that.authorsList.indexOf(author);
		that.authorsList.splice(authorIndex,1);
	}

	this.addTag = function(tag){
		that.tags.push(tag);
		var tagIndex = that.tagsList.indexOf(tag);
		that.tagsList.splice(tagIndex,1);
	} 

	this.removeAuthor = function(author){
		var authorIndex = that.authors.indexOf(author);
		that.authors.splice(authorIndex,1);
		that.authorsList.push(author);
		that.authorsList.sort((a,b)=> stringComparator(a.lastName, b.lastName));
	}

	this.removeTag =  function(tag){
		var tagIndex = that.tags.indexOf(tag);
		that.tags.splice(tagIndex,1);
		that.tagsList.push(tag);
		that.tagsList.sort((a,b)=> stringComparator(a.tag, b.tag));
	}

	var stringComparator = function(a,b){
		if (a < b)
			return -1;
		if (a > b)
			return 1;
		return 0;
	}
}]);