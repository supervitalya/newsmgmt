angular.module('tagsList').controller('TagDialogController', ['$modalInstance','tag', 'windowTitle',
 function ($modalInstance, tag, windowTitle) {
	var that = this;
	
	if(tag!=null){
		this.tag = tag.tag;
	}else{
		this.tag = '';
	}

	this.windowTitle = windowTitle;
	
 	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.ok = function(){
		$modalInstance.close(true);
	}
	this.update = function(){
		$modalInstance.close(that.tag);
	}
}]);