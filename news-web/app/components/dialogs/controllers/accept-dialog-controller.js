angular.module('newsApp')
	.controller('AcceptDialogController', ['$modalInstance', 'windowTitle',
		function ($modalInstance, windowTitle) {
	this.windowTitle = windowTitle;

	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.ok = function(){
		$modalInstance.close(true);
	}
}]);