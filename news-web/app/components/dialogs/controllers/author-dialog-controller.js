angular.module('authorsList')
.controller('AuthorDialogController', ['$modalInstance','author', 'windowTitle',
	function ($modalInstance, author, windowTitle) {
	var that = this;

	if(author!=null){
		this.firstName = author.firstName;
		this.lastName = author.lastName;
	}else{
		author={};
	}

	this.windowTitle = windowTitle;

	this.cancel = function() {
		$modalInstance.close(false);
	}
	this.save = function(){
		author.firstName = that.firstName;
		author.lastName = that.lastName;
		$modalInstance.close(author);
	}
}]);