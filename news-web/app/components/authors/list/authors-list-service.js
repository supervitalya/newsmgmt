angular.module('authorsList')
.factory('AuthorService',['$resource','DEFAULT_URL', function ($resource, DEFAULT_URL){
	return $resource(DEFAULT_URL+'/authors/:id', { id: '@id' }, {
		update: {
			method: 'PUT'
		}
	},
	{
		delete: {
			method: 'DELETE'
		}
	});
}]);
