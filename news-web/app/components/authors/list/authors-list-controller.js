angular.
module('authorsList')
.component('authorsList', {
	templateUrl: 'components/authors/list/authors-list-template.html',
	controller: ['AuthorService','$location','$modal', '$http','DEFAULT_URL',
	function AuthorsListController(AuthorService, $location, $modal, $http, DEFAULT_URL){
		
		var that = this;
		
		if(!this.number){
			this.number = 10;
		}
		if(!this.page) {
			this.page = 1;
		}
		
		this.updateAuthorPage = function(){
			var fetchedAuthors = AuthorService.query({number: that.number, page:that.page},
				function(){
					that.authorsList = fetchedAuthors;
				},errorHandler);
			that.errorMessage = null;
			that.successMessage = null;
			$http.get(DEFAULT_URL+'/authors/count').then(
				function(result){
					that.authorsNumber = result.data;
				},errorHandler);
		}
		
		this.updateAuthorPage();

		this.saveAuthor = function(){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/author-dialog.template.html',
				controller: 'AuthorDialogController',
				controllerAs: '$ctrl',
				resolve:{
					author: function(){
						return null;
					},
					windowTitle: function(){
						return 'Add author'
					}
				}
			});
			dialog.result.then(acceptSave);
		}

		var acceptSave = function(data){
			if(data){
				var author={
				firstName : data.firstName,
				lastName : data.lastName
				};
				AuthorService.save(author, function(){
					that.updateAuthorPage();
					that.successMessage='Author has been saved';
				}, errorHandler);
			}
		}
		
		this.updateAuthor = function(author){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/author-dialog.template.html',
				controller: 'AuthorDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle:function(){
						return'Update author';
					},
					author: function(){
						return author;
					}
				}
			});
			dialog.result.then(acceptUpdate);
		}

		var acceptUpdate = function(data){
			if(data){
				AuthorService.update(data, function(){
					that.updateAuthorPage();
					that.successMessage='Author has been updated';
				}, errorHandler);
			}
		}

		this.deleteAuthor = function(author){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/accept-dialog.template.html',
				controller: 'AcceptDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Delete author'
					}
				}				
			});
			dialog.result.then(function(accepted){
				acceptDelete(accepted, author);
			});
		}

		function acceptDelete(accepted, author){
			if(accepted){
				AuthorService.delete(author, function(){
					that.updateAuthorPage();
					that.successMessage='Author has been deleted.';
				},errorHandler);
			}
		}

		var errorHandler = function (error){
			switch(error.status){
				case 409:
					that.successMessage=null;
					that.errorMessage = error.data.message;
					break;
				case 422:
					that.successMessage=null;
					that.errorMessage = error.data.message;
					break;
				case 423:
					that.updateAuthorPage();
					that.errorMessage = error.data.message;
					break;
				default:
					$location.path('/error/'+error.status);
			}
		}
	}]
});