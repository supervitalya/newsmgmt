angular.
module('errorPage')
.component('errorPage', {
	templateUrl: 'components/error-page/error-page-template.html',
	controller: ['$routeParams', function ErrorPageController($routeParams){
		this.errorCode = $routeParams.code;
		var that = this;
		switch(this.errorCode){
			case '500':
				that.message = 'Internal server error. Please try again later. Sorry for the inconvenience.';
				break;
			case '400':
				that.message = 'Requested resource not found. Please, check the request.';
				break;
			default:
				that.message = 'Unknown error.';
		}
	}
	]
});