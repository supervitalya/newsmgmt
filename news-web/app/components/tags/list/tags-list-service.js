angular.module('tagsList')
.factory('TagService', ['$resource','DEFAULT_URL', function($resource, DEFAULT_URL){
	return $resource(DEFAULT_URL+'/tags/:tag', { tag: '@oldTag' }, {
	    update: {
	     	method: 'PUT'
        }
	},
	{
	    delete: {
	      	method: 'DELETE'
	    }
	});
}]);