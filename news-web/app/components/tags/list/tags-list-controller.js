angular.
module('tagsList')
.component('tagsList', {
	templateUrl: 'components/tags/list/tags-list-template.html',
	controller: ['TagService','DEFAULT_URL','$location','$modal', '$http',
	function TagsListController(TagService, DEFAULT_URL, $location, $modal, $http){
		
		var that = this;
		
		if(!this.number){
			this.number = 10;
		}
		if(!this.page) {
			this.page = 1;
		}
				
		this.updateTagPage = function(){
			var fetchedTags = TagService.query({number: that.number, page:that.page},
				function(){
					that.tagsList = fetchedTags;
				},errorHandler);
			that.errorMessage = null;
			that.successMessage = null;
			$http.get(DEFAULT_URL+'/tags/count').then(
				function(result){
					that.tagsNumber = result.data;
				},errorHandler);
		}

		this.updateTagPage();
		
		this.saveTag = function(){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/tag-dialog.template.html',
				controller: 'TagDialogController',
				controllerAs: '$ctrl',
				resolve:{
					tag: function(){
						return null;
					},
					windowTitle: function(){
						return 'Add tag';
					}
				}
			});
			dialog.result.then(acceptSave);
		}

		var acceptSave = function(tag){
			if(tag){
				TagService.save({'tag':tag}, function(){
					that.updateTagPage();
					that.successMessage='Tag has been saved';
				},errorHandler);
			}
		}

		this.deleteTag = function(tag){
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/accept-dialog.template.html',
				controller: 'AcceptDialogController',
				controllerAs: '$ctrl',
				resolve: {
					windowTitle: function(){
						return 'Delete tag';
					}
				}
			});
			dialog.result.then(function (isDeleted){
				if(isDeleted){
					TagService.delete(tag, function(){
						that.updateTagPage();
						that.successMessage='Tag has been deleted';
					}, errorHandler);
				}
			});
		}
		
		this.updateTag = function(tag){
			var oldTag = tag.tag;
			var dialog = $modal.open({
				templateUrl: 'components/dialogs/templates/tag-dialog.template.html',
				controller: 'TagDialogController',
				controllerAs: '$ctrl',
				resolve: {
					tag: function (){
						return tag;
					},
					windowTitle: function(){
						return 'Update tag';
					}
				}
			});

			dialog.result.then(function (newTag){
				if (newTag && (newTag != oldTag)){
					var putTagUrl = DEFAULT_URL+'/tags/'+oldTag;
					$http.put(putTagUrl,{'tag':newTag}).then( function(){
						that.updateTagPage();
						that.successMessage='Tag has been updated';
					},errorHandler);
				}
			});
		}

		var errorHandler = function (error){
					switch(error.status){
				case 409:
					that.updateTagPage();
					that.errorMessage = error.data.message;
					break;
				case 422:
					that.updateTagPage();
					that.errorMessage = 'Tag is invalid';
					break;
				case 423:
					that.updateTagPage();
					that.errorMessage = error.data.message;
					break;
				default:
					$location.path('/error/'+error.status);
			}
		}
	}]
});