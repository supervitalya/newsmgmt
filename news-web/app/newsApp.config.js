var newsApp = angular.
module('newsApp').
config(['$routeProvider',
	    function ($routeProvider) {
		$routeProvider
		.when('/news',{
			template: '<news-list></news-list>'
		})
		.when('/news/:id',{
			template: '<news-view></news-view>'
		})
		.when('/authors',{
			template: '<authors-list></authors-list>'	
		})
		.when('/tags',{
			template: '<tags-list></tags-list>'	
		})
		.when('/error/:code',{
			template: '<error-page></error-page>'
		})
		.otherwise('/news');
	}
]);

newsApp.run(['$rootScope','$location', function($rootScope, $location){
	var history = [];
  	$rootScope.showBackButton = false;
    $rootScope.$on('$routeChangeSuccess', function() {
  		history.push($location.$$path);
  		if(history.length>1){
  			$rootScope.showBackButton = true;
  		}

        if(history.length > 2){
	        history = history.slice(-2);
	    }
    });

   	$rootScope.back = function() {
        var previousUrl = history.length > 1 ? history[history.length-2] : "/";
        $location.path(previousUrl);
    };
}]);