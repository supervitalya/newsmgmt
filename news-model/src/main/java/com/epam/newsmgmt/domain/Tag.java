package com.epam.newsmgmt.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "TAGS")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Tag {
	@Id
	@Column(name = "TAG", unique = true, nullable = false)
	private String tag;

	@Transient
	private Long newsNumber;

	public Tag() {
		super();
	}

	public Tag(String tag) {
		super();
		this.tag = tag;
	}

	public Tag(String tag, Long newsNumber) {
		super();
		this.tag = tag;
		this.newsNumber = newsNumber;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Long getNewsNumber() {
		return newsNumber;
	}

	public void setNewsNumber(Long newsNumber) {
		this.newsNumber = newsNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tag [tag=" + tag + ", newsNumber=" + newsNumber + "]";
	}

}
