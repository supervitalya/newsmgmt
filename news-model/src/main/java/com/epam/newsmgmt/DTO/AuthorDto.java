package com.epam.newsmgmt.dto;

import java.io.Serializable;
import java.util.UUID;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AuthorDto implements Serializable {
	private static final long serialVersionUID = 629044311908621422L;

	private UUID id;

	@NotNull
	@Size(min = 2, max = 20)
	private String firstName;

	@NotNull
	@Size(min = 2, max = 20)
	private String lastName;

	private Long newsNumber;

	private int version;

	public AuthorDto() {
		super();
	}

	public AuthorDto(UUID id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public AuthorDto(UUID id, String firstName, String lastName, Long newsNumber) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.newsNumber = newsNumber;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getNewsNumber() {
		return newsNumber;
	}

	public void setNewsNumber(Long newsNumber) {
		this.newsNumber = newsNumber;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
