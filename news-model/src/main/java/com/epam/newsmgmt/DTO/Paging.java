package com.epam.newsmgmt.dto;

import javax.validation.constraints.Min;

public class Paging {
	@Min(1)
	private int page;
	@Min(1)
	private int number;

	public Paging() {
		super();
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
