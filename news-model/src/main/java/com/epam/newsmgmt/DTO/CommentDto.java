package com.epam.newsmgmt.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDto implements Serializable {
	private static final long serialVersionUID = -8008135580003384845L;

	private UUID id;

	@NotNull
	@Size(min = 1, max = 500)
	private String commentText;

	private Timestamp creationDate;

	private UUID newsId;

	public CommentDto() {
		super();
	}

	public CommentDto(UUID id, String commentText, Timestamp creationDate, UUID newsId) {
		super();
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public UUID getNewsId() {
		return newsId;
	}

	public void setNewsId(UUID newsId) {
		this.newsId = newsId;
	}
}
