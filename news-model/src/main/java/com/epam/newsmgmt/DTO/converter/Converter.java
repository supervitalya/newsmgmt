package com.epam.newsmgmt.dto.converter;

import java.io.Serializable;

public interface Converter<E, D extends Serializable> {
	E fromDto(D dto);

	D toDto(E entity);
}
