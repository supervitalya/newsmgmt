package com.epam.newsmgmt.dto.converter;

import org.springframework.stereotype.Component;

import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.dto.AuthorDto;

@Component
public class AuthorConverter implements Converter<Author, AuthorDto> {
	public AuthorDto toDto(Author author) {
		AuthorDto authorDto = new AuthorDto();
		authorDto.setId(author.getId());
		authorDto.setFirstName(author.getFirstName());
		authorDto.setLastName(author.getLastName());
		authorDto.setNewsNumber(author.getNewsNumber());
		authorDto.setVersion(author.getVersion());
		return authorDto;
	}

	public Author fromDto(AuthorDto authorDto) {
		Author author = new Author();
		author.setId(authorDto.getId());
		author.setFirstName(authorDto.getFirstName());
		author.setLastName(authorDto.getLastName());
		author.setNewsNumber(authorDto.getNewsNumber());
		author.setVersion(authorDto.getVersion());
		return author;
	}
}
