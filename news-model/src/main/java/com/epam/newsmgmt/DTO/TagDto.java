package com.epam.newsmgmt.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class TagDto implements Serializable {

	private static final long serialVersionUID = 588086888250071155L;

	@NotNull
	@Size(min = 2, max = 20)
	@Pattern(regexp = "[A-zА-я]+")
	private String tag;

	private Long newsNumber;

	public TagDto() {
		super();
	}

	public TagDto(String tag) {
		super();
		this.tag = tag;
	}

	public TagDto(String tag, Long newsNumber) {
		super();
		this.tag = tag;
		this.newsNumber = newsNumber;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Long getNewsNumber() {
		return newsNumber;
	}

	public void setNewsNumber(Long newsNumber) {
		this.newsNumber = newsNumber;
	}
}
