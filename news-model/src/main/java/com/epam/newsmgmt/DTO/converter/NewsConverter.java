package com.epam.newsmgmt.dto.converter;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.domain.Comment;
import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.domain.Tag;
import com.epam.newsmgmt.dto.AuthorDto;
import com.epam.newsmgmt.dto.CommentDto;
import com.epam.newsmgmt.dto.NewsDto;
import com.epam.newsmgmt.dto.TagDto;

@Component
public class NewsConverter implements Converter<News, NewsDto> {
	@Autowired
	private Converter<Author, AuthorDto> authorConverter;

	@Autowired
	private Converter<Tag, TagDto> tagConverter;

	@Autowired
	private Converter<Comment, CommentDto> commentConverter;

	@Override
	public NewsDto toDto(News news) {
		NewsDto newsDto = new NewsDto();
		newsDto.setId(news.getId());
		newsDto.setTitle(news.getTitle());
		newsDto.setShortText(news.getShortText());
		newsDto.setFullText(news.getFullText());
		newsDto.setCreationDate(news.getCreationDate());
		newsDto.setModificationDate(news.getModificationDate());
		newsDto.setVersion(news.getVersion());
		newsDto.setAuthors(
				news.getAuthors().stream().map(author -> authorConverter.toDto(author)).collect(Collectors.toList()));
		newsDto.setTags(news.getTags().stream().map(tag -> tagConverter.toDto(tag)).collect(Collectors.toList()));
		if (newsDto.getComments() != null) {
			newsDto.setComments(news.getComments().stream().map(comment -> commentConverter.toDto(comment))
					.collect(Collectors.toList()));
		}
		return newsDto;
	}

	@Override
	public News fromDto(NewsDto newsDto) {
		News news = new News();
		news.setId(newsDto.getId());
		news.setTitle(newsDto.getTitle());
		news.setShortText(newsDto.getShortText());
		news.setFullText(newsDto.getFullText());
		news.setCreationDate(newsDto.getCreationDate());
		news.setModificationDate(newsDto.getModificationDate());
		news.setVersion(newsDto.getVersion());
		news.setAuthors(newsDto.getAuthors().stream().map(authorDto -> authorConverter.fromDto(authorDto))
				.collect(Collectors.toList()));
		news.setTags(
				newsDto.getTags().stream().map(tagDto -> tagConverter.fromDto(tagDto)).collect(Collectors.toList()));
		if (news.getComments() != null) {
			news.setComments(newsDto.getComments().stream().map(CommentDto -> commentConverter.fromDto(CommentDto))
					.collect(Collectors.toList()));
		}
		return news;
	}

}
