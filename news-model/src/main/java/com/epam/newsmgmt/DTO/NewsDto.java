package com.epam.newsmgmt.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class NewsDto implements Serializable {

	private static final long serialVersionUID = 801770656505751011L;

	private UUID id;

	@NotNull
	@Size(min = 5, max = 200)
	private String title;

	@NotNull
	@Size(min = 10, max = 200)
	private String shortText;

	@NotNull
	@Size(min = 50, max = 2000)
	private String fullText;

	private Timestamp creationDate;

	private Timestamp modificationDate;

	private List<AuthorDto> authors;

	private List<TagDto> tags;

	@JsonIgnore
	private List<CommentDto> comments;

	private int version;

	public NewsDto() {
		super();
	}

	public NewsDto(UUID id, String title, String shortText, String fullText, Timestamp creationDate,
			Timestamp modificationDate) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Timestamp modificationDate) {
		this.modificationDate = modificationDate;
	}

	public List<AuthorDto> getAuthors() {
		return authors;
	}

	public void setAuthors(List<AuthorDto> author) {
		this.authors = author;
	}

	public List<TagDto> getTags() {
		return tags;
	}

	public void setTags(List<TagDto> tags) {
		this.tags = tags;
	}

	public List<CommentDto> getComments() {
		return comments;
	}

	public void setComments(List<CommentDto> comments) {
		this.comments = comments;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
