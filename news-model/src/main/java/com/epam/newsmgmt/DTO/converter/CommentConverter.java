package com.epam.newsmgmt.dto.converter;

import org.springframework.stereotype.Component;

import com.epam.newsmgmt.domain.Comment;
import com.epam.newsmgmt.dto.CommentDto;

@Component
public class CommentConverter implements Converter<Comment, CommentDto> {

	public CommentDto toDto(Comment comment) {
		CommentDto commentDto = new CommentDto();
		commentDto.setId(comment.getId());
		commentDto.setCommentText(comment.getCommentText());
		commentDto.setCreationDate(comment.getCreationDate());
		commentDto.setNewsId(comment.getNewsId());
		return commentDto;
	}

	public Comment fromDto(CommentDto commentDto) {
		Comment comment = new Comment();
		comment.setId(commentDto.getId());
		comment.setCommentText(commentDto.getCommentText());
		comment.setCreationDate(commentDto.getCreationDate());
		comment.setNewsId(commentDto.getNewsId());
		return comment;
	}
}
