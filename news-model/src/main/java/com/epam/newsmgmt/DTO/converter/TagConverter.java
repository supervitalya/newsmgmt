package com.epam.newsmgmt.dto.converter;

import org.springframework.stereotype.Component;

import com.epam.newsmgmt.domain.Tag;
import com.epam.newsmgmt.dto.TagDto;

@Component
public class TagConverter implements Converter<Tag, TagDto> {
	public TagDto toDto(Tag tag) {
		return new TagDto(tag.getTag(), tag.getNewsNumber());
	}

	public Tag fromDto(TagDto tagDto) {
		return new Tag(tagDto.getTag(), tagDto.getNewsNumber());
	}
}
