create table news_tag(
 news_tag_id RAW(16) primary key default SYS_GUID(),
 news RAW(16) not null,
 tag varchar2(20) not null,
 
 CONSTRAINT fk_news
    FOREIGN KEY (news)
    REFERENCES news (news_id),
    
    CONSTRAINT fk_tag
    FOREIGN KEY (tag)
    REFERENCES tags (tag)
);