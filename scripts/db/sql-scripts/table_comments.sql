create table comments(
  comment_id RAW(16) primary key default SYS_GUID(),
  comment_text varchar2(50) not null,
  creation_date timestamp,
  news RAW(16)
  
 	CONSTRAINT NEWS_FK
    FOREIGN KEY (news)
    REFERENCES news (news_id),
);