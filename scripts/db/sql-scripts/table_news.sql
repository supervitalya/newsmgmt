create table news(
  news_id RAW(16) primary key default SYS_GUID(),
  title varchar2(50),
  short_text varchar2(100),
  full_text varchar2(2000),
  creation_date timestamp,
  modification_date timestamp
);