create table news_author(
 news_author_id RAW(16) primary key default SYS_GUID(),
 news RAW(16) not null,
 author RAW(16) not null,
 
 	CONSTRAINT fk_na_news
    FOREIGN KEY (news)
    REFERENCES news (news_id),
    
    CONSTRAINT fk_na_author
    FOREIGN KEY (author)
    REFERENCES authors (author_id)
);
