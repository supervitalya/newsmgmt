create table authors(
 author_id RAW(16) primary key default SYS_GUID(),
 first_name varchar(50) not null,
 second_name varchar(50) not null,
);
