create table authors(
 author_id RAW(16) primary key default SYS_GUID(),
 first_name varchar(50) not null,
 second_name varchar(50) not null,
);

create table comments(
  comment_id RAW(16) primary key default SYS_GUID(),
  comment_text varchar2(50) not null,
  creation_date timestamp,
  news RAW(16)
  
 	CONSTRAINT NEWS_FK
    FOREIGN KEY (news)
    REFERENCES news (news_id),
);

create table news(
  news_id RAW(16) primary key default SYS_GUID(),
  title varchar2(50),
  short_text varchar2(100),
  full_text varchar2(2000),
  creation_date timestamp,
  modification_date timestamp
);

create table tags(
tag varchar2(20) primary key
);

create table news_author(
 news_author_id RAW(16) primary key default SYS_GUID(),
 news RAW(16) not null,
 author RAW(16) not null,
 
 	CONSTRAINT fk_na_news
    FOREIGN KEY (news)
    REFERENCES news (news_id),
    
    CONSTRAINT fk_na_author
    FOREIGN KEY (author)
    REFERENCES authors (author_id)
);

create table news_tag(
 news_tag_id RAW(16) primary key default SYS_GUID(),
 news RAW(16) not null,
 tag varchar2(20) not null,
 
 CONSTRAINT fk_news
    FOREIGN KEY (news)
    REFERENCES news (news_id),
    
    CONSTRAINT fk_tag
    FOREIGN KEY (tag)
    REFERENCES tags (tag)
);