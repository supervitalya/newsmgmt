package com.epam.newsmgmt.dao;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.domain.Tag;

/**
 * Provides access to database for operations with Tag entity.
 * 
 * @author Vitali_Bliakharchuk
 *
 */
public interface TagDao {
	/**
	 * Adds Tag entity.
	 * 
	 * @param tag
	 *            entity
	 * @return inserted Tag entity.
	 */
	Tag insert(Tag tag);

	/**
	 * Updates Tag entity.
	 * 
	 * @param oldTag
	 *            replaced Tag entity.
	 * @param newTag
	 *            new Tag entity.
	 */
	void update(Tag oldTag, Tag newTag);

	/**
	 * Retrieves list of Tag entities from specified position. Tags number into
	 * list can be up to maxResults.
	 * 
	 * @param startFrom
	 *            start fetching position.
	 * @param maxResults
	 *            maximal entities number.
	 * @return list of Tag entities.
	 */
	List<Tag> fetchAll(int startFrom, int maxResults);

	/**
	 * Retrieves list of all Tag entities.
	 * 
	 * @return list of all Tag entities.
	 */
	List<Tag> fetchAll();

	/**
	 * Retrieves list of Tag entities for News with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return list of Tag entities.
	 */
	List<Tag> fetchByNewsId(UUID newsId);

	/**
	 * Removes specified Tag entity.
	 * 
	 * @param tag
	 *            entity which should be removed
	 * @return removed Tag entity
	 */
	Tag remove(Tag tag);

	/**
	 * Retrieves number of all Tag entities.
	 * 
	 * @return Tag entities number.
	 */
	long fetchAllTagsNumber();
}
