package com.epam.newsmgmt.dao.hibernate;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmgmt.dao.NewsDao;
import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.domain.Tag;

@Repository
@Transactional
public class NewsDaoImpl implements NewsDao {

	private static final String HQL_FETCH_BY_TAG = "select distinct n from News n join n.tags tag "
			+ "where tag.tag=:tag ORDER BY n.creationDate desc";

	private static final String HQL_FETCH_BY_TAG_COUNT = "select count(n) from News n join n.tags tag "
			+ "where tag.tag=:tag";

	private static final String HQL_FETCH_BY_AUTHOR = "select distinct n from News n join n.authors "
			+ "auth where auth.id=:authorId ORDER BY n.creationDate desc";

	private static final String HQL_FETCH_BY_AUTHOR_COUNT = "select count(n) from News n join n.authors "
			+ "auth where auth.id=:authorId";

	private static final String HQL_FETCH_TOP_BY_COMMENTS = "SELECT n FROM News n LEFT JOIN "
			+ "n.comments AS coms GROUP BY n.id, n.title, n.shortText, n.fullText, n.creationDate, "
			+ "n.modificationDate, n.version ORDER BY COUNT(coms) DESC, n.creationDate desc";

	private static final String TAG_ENTITY_NAME = "tag";
	private static final String NEWS_FIELD_CREATION_DATE = "creationDate";
	private static final String AUTHOR_FIELD_ID = "authorId";

	@Autowired
	private SessionFactory factory;

	@Override
	public News fetchById(UUID newsId) {
		return factory.getCurrentSession().get(News.class, newsId);
	}

	@Override
	public void update(News news) {
		factory.getCurrentSession().update(news);
	}

	@Override
	public News remove(UUID newsId) {
		Session session = factory.getCurrentSession();
		News news = session.get(News.class, newsId);
		session.delete(news);
		return news;
	}

	@Override
	public News insert(News news) {
		Session session = factory.getCurrentSession();
		session.persist(news);
		session.flush();
		return news;
	}

	@Override
	public List<News> fetchAll(int startFrom, int maxResults) {
		Criteria criteria = factory.getCurrentSession().createCriteria(News.class)
				.addOrder(Order.desc(NEWS_FIELD_CREATION_DATE));
		criteria.setFirstResult(startFrom);
		criteria.setMaxResults(maxResults);
		return criteria.list();
	}

	@Override
	public List<News> fetchByAuthorId(UUID authorId, int startFrom, int maxResults) {
		Query query = factory.getCurrentSession().createQuery(HQL_FETCH_BY_AUTHOR);
		query.setParameter(AUTHOR_FIELD_ID, authorId);
		query.setFirstResult(startFrom);
		query.setMaxResults(maxResults);
		return query.list();
	}

	@Override
	public List<News> fetchByTag(Tag tag, int startFrom, int maxResults) {
		Query query = factory.getCurrentSession().createQuery(HQL_FETCH_BY_TAG);
		query.setParameter(TAG_ENTITY_NAME, tag.getTag());
		query.setFirstResult(startFrom);
		query.setMaxResults(maxResults);
		return query.list();
	}

	@Override
	public List<News> fetchSortedByComments(int startFrom, int maxResults) {
		Query query = factory.getCurrentSession().createQuery(HQL_FETCH_TOP_BY_COMMENTS);
		query.setFirstResult(startFrom);
		query.setMaxResults(maxResults);
		return query.list();
	}

	@Override
	public long fetchCountAll() {
		Criteria criteria = factory.getCurrentSession().createCriteria(News.class);
		Object o = criteria.setProjection(Projections.rowCount()).uniqueResult();
		return (long) o;
	}

	@Override
	public long fetchCountByAuthorID(UUID authorId) {
		Query query = factory.getCurrentSession().createQuery(HQL_FETCH_BY_AUTHOR_COUNT);
		query.setParameter(AUTHOR_FIELD_ID, authorId);
		return (long) query.uniqueResult();
	}

	@Override
	public long fetchCountByTag(Tag tag) {
		Query query = factory.getCurrentSession().createQuery(HQL_FETCH_BY_TAG_COUNT);
		query.setParameter(TAG_ENTITY_NAME, tag.getTag());
		return (long) query.uniqueResult();
	}

}
