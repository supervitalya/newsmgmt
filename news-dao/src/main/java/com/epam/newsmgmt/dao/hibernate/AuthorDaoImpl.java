package com.epam.newsmgmt.dao.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.domain.Author;

@Repository
@Transactional
public class AuthorDaoImpl implements AuthorDao {

	private static final String HQL_FETCH_AUTHORS_WITH_NEWS_NUMBER = "select a, count(n) as countNews "
			+ "from News n right join n.authors a group by a.id, a.firstName, a.lastName, a.version order by a.lastName, a.firstName";

	private static final String AUTHOR_FIELD_LAST_NAME = "lastName";
	private static final String AUTHOR_FIELD_FIRST_NAME = "firstName";

	@Autowired
	private SessionFactory factory;

	@Override
	public Author insert(Author author) {
		Session session = factory.getCurrentSession();
		session.persist(author);
		session.flush();
		return author;
	}

	@Override
	public Author fetchById(UUID authorId) {
		return factory.getCurrentSession().get(Author.class, authorId);
	}

	@Override
	public void update(Author author) {
		factory.getCurrentSession().update(author);
	}

	@Override
	public List<Author> fetchAll(int startFrom, int maxResults) {
		Criteria criteria = factory.getCurrentSession().createCriteria(Author.class)
				.addOrder(Order.asc(AUTHOR_FIELD_LAST_NAME)).addOrder(Order.asc(AUTHOR_FIELD_FIRST_NAME));
		criteria.setFirstResult(startFrom);
		criteria.setMaxResults(maxResults);
		return criteria.list();
	}

	@Override
	public Author remove(UUID authorId) {
		Session session = factory.getCurrentSession();
		Author author = session.get(Author.class, authorId);
		session.delete(author);
		return author;
	}

	@Override
	public List<Author> fetchAll() {
		Query query = factory.getCurrentSession().createQuery(HQL_FETCH_AUTHORS_WITH_NEWS_NUMBER);
		List<Object> authorsWithNumber = query.list();
		return objectsListToAuthorsList(authorsWithNumber);
	}

	@Override
	public long fetchAllAuthorsNumber() {
		Criteria criteria = factory.getCurrentSession().createCriteria(Author.class)
				.setProjection(Projections.rowCount());
		return (long) criteria.uniqueResult();
	}

	private List<Author> objectsListToAuthorsList(List<Object> objectsList) {
		List<Author> resultList = new ArrayList<>();
		for (Object pair : objectsList) {
			Object[] authorWithCountPair = (Object[]) pair;
			Author author = (Author) authorWithCountPair[0];
			author.setNewsNumber((Long) authorWithCountPair[1]);
			resultList.add(author);
		}
		return resultList;
	}
}
