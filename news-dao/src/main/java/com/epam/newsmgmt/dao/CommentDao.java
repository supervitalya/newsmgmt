package com.epam.newsmgmt.dao;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.domain.Comment;

/**
 * Provides access to database for operations with Comment entity.
 * 
 * @author Vitali_Bliakharchuk
 *
 */
public interface CommentDao {
	/**
	 * Retrieves list of Comment entities for News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @param startFrom
	 *            fetching start position.
	 * @param maxResults
	 *            maximal number of fetching Comment entities.
	 * @return list of Comment entities for the News.
	 */
	List<Comment> fetchByNewsId(UUID newsId, int startFrom, int maxResults);

	/**
	 * Retrieves list of Comment entities for News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return list of Comment entities for the News.
	 */
	List<Comment> fetchByNewsId(UUID newsId);

	/**
	 * Removes Comment entity by it's id. Returns removed entity.
	 * 
	 * @param commentId
	 *            Comment entity id.
	 * @return removed Comment entity.
	 */
	Comment remove(UUID commentId);

	/**
	 * Updates Comment entity.
	 * 
	 * @param comment
	 *            entity
	 */
	void update(Comment comment);

	/**
	 * Adds Comment entity. Returns it with generated id.
	 * 
	 * @param comment
	 *            entity.
	 * @return inserted entity with generated id.
	 */
	Comment insert(Comment comment);

	/**
	 * Retrieves number of comments for News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return number of comments
	 */
	long fetchAllCommentsNumberByNewsId(UUID newsId);
}
