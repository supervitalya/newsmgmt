package com.epam.newsmgmt.dao;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.domain.Tag;

/**
 * Provides access to database for operations with Author entity.
 * 
 * @author Vitali_Bliakharchuk
 *
 */
public interface NewsDao {
	/**
	 * Retrieves News entity by it's id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return News entity.
	 */
	News fetchById(UUID newsId);

	/**
	 * Updates News entity.
	 * 
	 * @param news
	 *            updated entity.
	 */
	void update(News news);

	/**
	 * Removes News entity with specified id.
	 * 
	 * @param newsId
	 *            News entity id.
	 * @return removed News entity.
	 */
	News remove(UUID newsId);

	/**
	 * Adds News entity. Returns entity with generated id.
	 * 
	 * @param news
	 *            entity
	 * @return inserted News entity with generated id.
	 */
	News insert(News news);

	/**
	 * Retrieves News entities starting from specified position. Maximal number
	 * of entities in list can be up to maxResults.
	 * 
	 * @param startFrom
	 *            fetching start position.
	 * @param maxResults
	 *            maximal number of News in list.
	 * @return list of News entities.
	 */
	List<News> fetchAll(int startFrom, int maxResults);

	/**
	 * Retrieves number of all News entities.
	 * 
	 * @return number of News entities
	 */
	long fetchCountAll();

	/**
	 * Retrieves News entities by their Author id.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @param startFrom
	 *            fetching start position.
	 * @param maxResults
	 *            maximal number of News entities in list.
	 * @return list of News entities.
	 */
	List<News> fetchByAuthorId(UUID authorId, int startFrom, int maxResults);

	/**
	 * Retrieves number of News entities related to Author entity with specified
	 * id. If there's no News related to Author with specified id returns 0.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @return number of News.
	 */
	long fetchCountByAuthorID(UUID authorId);

	/**
	 * Retrieves News entities by their Tag.
	 * 
	 * @param tag
	 *            tag entity.
	 * @param startFrom
	 *            fetching start position.
	 * @param maxResults
	 *            maximal number of News entities in list.
	 * @return list of News entities.
	 */
	List<News> fetchByTag(Tag tag, int startFrom, int maxResults);

	/**
	 * Retrieves number of News entities related to Tag entity. If there's no
	 * News related to specified Tag returns 0.
	 * 
	 * @param tag
	 *            entity
	 * @return number of News entities.
	 */
	long fetchCountByTag(Tag tag);

	/**
	 * Retrieves list of News entities sorted by number of comments.
	 * 
	 * @param startFrom
	 *            fetching start position.
	 * @param maxResults
	 *            maximal number of News in list.
	 * @return list of News entities.
	 */
	List<News> fetchSortedByComments(int startFrom, int maxResults);

}
