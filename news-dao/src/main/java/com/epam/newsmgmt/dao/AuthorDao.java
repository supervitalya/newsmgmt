package com.epam.newsmgmt.dao;

import java.util.List;
import java.util.UUID;

import com.epam.newsmgmt.domain.Author;

/**
 * Provides access to database for operations with Author entity.
 * 
 * @author Vitali_Bliakharchuk
 *
 */
public interface AuthorDao {
	/**
	 * Adds author entity with generated id.
	 * 
	 * @param author
	 *            entity
	 * @return inserted Author entity with generated id.
	 */
	Author insert(Author author);

	/**
	 * Retrieves Author entity by it's id.
	 * 
	 * @param authorId
	 *            Author entity id
	 * @return
	 */
	Author fetchById(UUID authorId);

	/**
	 * Updates Author entity.
	 * 
	 * @param author
	 *            entity
	 */
	void update(Author author);

	/**
	 * Retrieves all entity from specified position with specified amount.
	 * 
	 * @param startFrom
	 *            position fetching starts with.
	 * @param maxResults
	 *            maximal number of fetched entities.
	 * @return list of Author entities
	 */
	List<Author> fetchAll(int startFrom, int maxResults);

	/**
	 * Retrieves all entities. Each Author entity will contain news number
	 * related to it.
	 * 
	 * @return list of Author entities.
	 */
	List<Author> fetchAll();

	/**
	 * Removes entity by it's id.
	 * 
	 * @param authorId
	 *            Author entity id.
	 * @return deleted entity.
	 */
	Author remove(UUID authorId);

	/**
	 * Retrieves number of all Author entities.
	 * 
	 * @return number of all Author entities.
	 */
	long fetchAllAuthorsNumber();
}
