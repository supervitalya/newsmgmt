package com.epam.newsmgmt.dao.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmgmt.dao.TagDao;
import com.epam.newsmgmt.domain.Tag;

@Repository
@Transactional
public class TagDaoImpl implements TagDao {

	private static final String HQL_SELECT_TAG_BY_NEWS_ID = "select distinct t from Tag t "
			+ "join t.news news where news.id=:newsId order by t.tag";

	private static final String HQL_SELECT_TAG_WITH_NEWS_NUMBER = "select t, count(n) as "
			+ "countNews from News n right join n.tags t group by t order by t.tag";

	private static final String SQL_TAG_INSERT = "insert into TAGS(TAG) values(?)";

	private static final String SQL_UPDATE_NEWS_TAG_CONNECTION = "update NEWS_TAG set "
			+ "NEWS_TAG.TAG=? where NEWS_TAG.TAG=?";

	private static final String SQL_TAG_DELETE = "delete from TAGS where TAGS.TAG=?";

	private static final String TAG_FIELD = "tag";

	private static final String TAG_FIELD_NEWS_ID = "newsId";

	@Autowired
	private SessionFactory factory;

	@Override
	public Tag insert(Tag tag) {
		Session session = factory.getCurrentSession();
		session.save(tag);
		return tag;
	}

	@Override
	public void update(Tag oldTag, Tag newTag) {

		Session session = factory.getCurrentSession();
		Query insertTagQuery = session.createSQLQuery(SQL_TAG_INSERT);
		insertTagQuery.setString(0, newTag.getTag());
		insertTagQuery.executeUpdate();

		Query updateLinksQuery = session.createSQLQuery(SQL_UPDATE_NEWS_TAG_CONNECTION);
		updateLinksQuery.setString(0, newTag.getTag());
		updateLinksQuery.setString(1, oldTag.getTag());
		updateLinksQuery.executeUpdate();

		Query updateTagsQuery = session.createSQLQuery(SQL_TAG_DELETE);
		updateTagsQuery.setString(0, oldTag.getTag());
		updateTagsQuery.executeUpdate();
	}

	@Override
	public List<Tag> fetchAll(int startFrom, int maxResults) {
		Criteria criteria = factory.getCurrentSession().createCriteria(Tag.class).addOrder(Order.asc(TAG_FIELD));
		criteria.setFirstResult(startFrom);
		criteria.setMaxResults(maxResults);
		return criteria.list();
	}

	@Override
	public Tag remove(Tag tag) {
		factory.getCurrentSession().delete(tag);
		return tag;
	}

	@Override
	public List<Tag> fetchByNewsId(UUID newsId) {
		Query query = factory.getCurrentSession().createQuery(HQL_SELECT_TAG_BY_NEWS_ID);
		query.setParameter(TAG_FIELD_NEWS_ID, newsId);
		List<Tag> tagsList = query.list();
		return tagsList;
	}

	@Override
	public List<Tag> fetchAll() {
		Query query = factory.getCurrentSession().createQuery(HQL_SELECT_TAG_WITH_NEWS_NUMBER);
		List<Object> tagsWithNumber = query.list();
		return objectsListToTagsList(tagsWithNumber);
	}

	@Override
	public long fetchAllTagsNumber() {
		Criteria criteria = factory.getCurrentSession().createCriteria(Tag.class).setProjection(Projections.rowCount());
		return (long) criteria.uniqueResult();
	}

	private List<Tag> objectsListToTagsList(List<Object> objectsList) {
		List<Tag> resultList = new ArrayList<>();
		for (Object pair : objectsList) {
			Object[] tagWithCountPair = (Object[]) pair;
			Tag tag = (Tag) tagWithCountPair[0];
			tag.setNewsNumber((Long) tagWithCountPair[1]);
			resultList.add(tag);
		}
		return resultList;
	}
}
