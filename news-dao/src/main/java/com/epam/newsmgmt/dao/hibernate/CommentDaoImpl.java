package com.epam.newsmgmt.dao.hibernate;

import java.util.List;
import java.util.UUID;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmgmt.dao.CommentDao;
import com.epam.newsmgmt.domain.Comment;

@Repository
@Transactional
public class CommentDaoImpl implements CommentDao {

	private static final String HQL_SELECT_COMMENT_BY_NEWS_ID = "select distinct c from Comment c join "
			+ "c.news news where news.id=:newsId order by c.creationDate desc";

	private static final String HQL_SELECT_COMMENT_BY_NEWS_ID_COUNT = "select count(c) from Comment c join "
			+ "c.news news where news.id=:newsId";

	private static final String FIELD_NEWS_ID = "newsId";

	@Autowired
	private SessionFactory factory;

	@Override
	public Comment remove(UUID commentId) {
		Session session = factory.getCurrentSession();
		Comment comment = session.get(Comment.class, commentId);
		session.delete(comment);
		return comment;
	}

	@Override
	public void update(Comment comment) {
		factory.getCurrentSession().update(comment);
	}

	@Override
	public Comment insert(Comment comment) {
		Session session = factory.getCurrentSession();
		session.persist(comment);
		session.flush();
		return comment;
	}

	@Override
	public List<Comment> fetchByNewsId(UUID newsId, int startFrom, int maxResults) {
		Query query = factory.getCurrentSession().createQuery(HQL_SELECT_COMMENT_BY_NEWS_ID);
		query.setParameter(FIELD_NEWS_ID, newsId);
		query.setFirstResult(startFrom);
		query.setMaxResults(maxResults);
		return query.list();
	}

	@Override
	public List<Comment> fetchByNewsId(UUID newsId) {
		Query query = factory.getCurrentSession().createQuery(HQL_SELECT_COMMENT_BY_NEWS_ID);
		query.setParameter(FIELD_NEWS_ID, newsId);
		return query.list();
	}

	@Override
	public long fetchAllCommentsNumberByNewsId(UUID newsId) {
		Query query = factory.getCurrentSession().createQuery(HQL_SELECT_COMMENT_BY_NEWS_ID_COUNT);
		query.setParameter(FIELD_NEWS_ID, newsId);
		return (long) query.uniqueResult();
	}

}
