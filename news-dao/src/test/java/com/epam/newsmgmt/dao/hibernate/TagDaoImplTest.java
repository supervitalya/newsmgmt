package com.epam.newsmgmt.dao.hibernate;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmgmt.dao.TagDao;
import com.epam.newsmgmt.domain.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/dao-context.xml" })

public class TagDaoImplTest {
	@Autowired
	TagDao tagDao;

	@Test
	public void fetchAllTagsNumberTest() {
		long number = tagDao.fetchAllTagsNumber();
		assertTrue(number != 0);
	}

	@Test
	public void fetchTagsWithNewsNumberTest() {
		List<Tag> tags = tagDao.fetchAll();
		tags.forEach(tag -> assertTrue(tag.getNewsNumber() != null));
	}
}
