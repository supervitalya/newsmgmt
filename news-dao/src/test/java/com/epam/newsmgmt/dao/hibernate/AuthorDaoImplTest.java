package com.epam.newsmgmt.dao.hibernate;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.domain.Author;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/dao-context.xml" })

public class AuthorDaoImplTest extends TestCase {
	private static final Logger LOG = Logger.getLogger(AuthorDaoImplTest.class);
	@Inject
	AuthorDao authorDao;

	/*
	 * I ignore this test because it really makes changes into database. Should
	 * be replaced with some database unit testing framework.
	 */
	@Ignore
	@Test
	public void saveAuthorTest() {
		Author author = new Author(null, "Name: " + String.valueOf(System.currentTimeMillis()), "Smith");
		Author saved = authorDao.insert(author);
		assertNotNull(saved);
		assertEquals(author.getFirstName(), saved.getFirstName());
	}

	/*
	 * I ignore this test because it really makes changes into database. Should
	 * be replaced with some database unit testing framework.
	 */
	@Ignore
	@Test
	public void deleteAuthorTest() {
		Author author = new Author(null, "Terminator: " + String.valueOf(System.currentTimeMillis()), "Smith");
		Author saved = authorDao.insert(author);
		assertNotNull(saved);
		UUID authorId = saved.getId();
		authorDao.remove(authorId);
		Author deleted = authorDao.fetchById(authorId);
		assertNull(deleted);
	}

	/*
	 * I ignore this test because it really makes changes into database. Should
	 * be replaced with some database unit testing framework.
	 */
	@Ignore
	@Test
	public void updateAuthorTest() {
		List<Author> authors = authorDao.fetchAll(1, 100);
		assertNotNull(authors);
		assertTrue(!authors.isEmpty());
		Author author = authors.get(0);
		String someUniqueName = "Name: " + String.valueOf(System.currentTimeMillis());
		author.setFirstName(someUniqueName);
		authorDao.update(author);
		Author updated = authorDao.fetchById(author.getId());
		assertEquals(someUniqueName, updated.getFirstName());
	}

	@Test
	public void fetchAllAuthorsTest() {
		List<Author> authors = authorDao.fetchAll(1, 3);
		assertTrue(authors.size() > 0);
		assertTrue(authors.size() <= 3);
	}

	@Test
	public void fetchAllAuthorsNumberTest() {
		long number = authorDao.fetchAllAuthorsNumber();
		assertTrue(number != 0);
	}

	@Test
	public void fetchAllAuthorsWithNewsNumberTest() {
		List<Author> authors = authorDao.fetchAll();
		authors.forEach(author -> assertTrue(author.getNewsNumber() != null));
	}

}
