package com.epam.newsmgmt.dao.hibernate;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmgmt.dao.AuthorDao;
import com.epam.newsmgmt.dao.CommentDao;
import com.epam.newsmgmt.dao.NewsDao;
import com.epam.newsmgmt.dao.TagDao;
import com.epam.newsmgmt.domain.Author;
import com.epam.newsmgmt.domain.News;
import com.epam.newsmgmt.domain.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/dao-context.xml" })

public class NewsDaoImplTest {
	@Autowired
	NewsDao newsDao;
	@Autowired
	AuthorDao authorDao;
	@Autowired
	TagDao tagDao;
	@Autowired
	CommentDao commentDao;

	@Test
	public void fetchNewsSortedByCommentsTest() {
		List<News> news = newsDao.fetchSortedByComments(1, 10);
		for (int i = 0; i < news.size() - 1; i++) {
			long firstNumber = commentDao.fetchAllCommentsNumberByNewsId(news.get(i).getId());
			long secondNumber = commentDao.fetchAllCommentsNumberByNewsId(news.get(i + 1).getId());
			assertTrue(firstNumber >= secondNumber);
		}
	}

	@Test
	public void fetchCountNewsAllTest() {
		long count = newsDao.fetchCountAll();
		assertTrue(count > 0);
	}

	@Test
	public void fetchCountNewsByAuthorIdTest() {
		Author author = authorDao.fetchAll(5, 1).get(0);
		long count = newsDao.fetchCountByAuthorID(author.getId());
		assertTrue(count > 0);
	}

	@Test
	public void fetchCountNewsByTagTest() {
		Tag tag = tagDao.fetchAll(6, 1).get(0);
		long count = newsDao.fetchCountByTag(tag);
		assertTrue(count > 0);
		Tag notExist = new Tag("UNIQUE TAG" + String.valueOf(System.currentTimeMillis()));
		long countNotExistTag = newsDao.fetchCountByTag(notExist);
		assertTrue(countNotExistTag == 0);
	}
}
